/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global eventSystem log */
/* eslint no-underscore-dangle: warn  */
/* eslint no-loop-func: warn */
/* eslint no-cond-assign: warn */
/* eslint no-unused-vars: warn */
/* eslint consistent-return: warn */
/* eslint import/no-dynamic-require: warn */
/* eslint global-require: warn */
/* eslint no-await-in-loop: warn */
/* eslint no-undef: warn */

/* Required libraries.  */
const fs = require('fs-extra');
const path = require('path');
const EventEmitterCl = require('events').EventEmitter;

const workbench = require(path.join(__dirname, 'adapterGrpcBase.js'));
const RequestHandlerCl = require('@itentialopensource/adapter-utils').RequestHandler;

// The schema validator
const AjvCl = require('ajv');

let myid = null;
let errors = [];

const onlyOne = (...args) => args.reduce((acc, v) => acc + (v !== null), 0) === 1;

/**
 * @summary Build a standard error object from the data provided
 *
 * @function formatErrorObject
 * @param {String} origin - the originator of the error (optional).
 * @param {String} type - the internal error type (optional).
 * @param {String} variables - the variables to put into the error message (optional).
 * @param {Integer} sysCode - the error code from the other system (optional).
 * @param {Object} sysRes - the raw response from the other system (optional).
 * @param {Exception} stack - any available stack trace from the issue (optional).
 *
 * @return {Object} - the error object, null if missing pertinent information
 */
function formatErrorObject(origin, type, variables, sysCode, sysRes, stack) {
  log.trace(`${myid}-adapter-formatErrorObject`);

  // add the required fields
  const errorObject = {
    icode: 'AD.999',
    IAPerror: {
      origin: `${myid}-unidentified`,
      displayString: 'error not provided',
      recommendation: 'report this issue to the adapter team!'
    }
  };

  if (origin) {
    errorObject.IAPerror.origin = origin;
  }
  if (type) {
    errorObject.IAPerror.displayString = type;
  }

  // add the messages from the error.json
  for (let e = 0; e < errors.length; e += 1) {
    if (errors[e].key === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    } else if (errors[e].icode === type) {
      errorObject.icode = errors[e].icode;
      errorObject.IAPerror.displayString = errors[e].displayString;
      errorObject.IAPerror.recommendation = errors[e].recommendation;
    }
  }

  // replace the variables
  let varCnt = 0;
  while (errorObject.IAPerror.displayString.indexOf('$VARIABLE$') >= 0) {
    let curVar = '';

    // get the current variable
    if (variables && Array.isArray(variables) && variables.length >= varCnt + 1) {
      curVar = variables[varCnt];
    }
    varCnt += 1;
    errorObject.IAPerror.displayString = errorObject.IAPerror.displayString.replace('$VARIABLE$', curVar);
  }

  // add all of the optional fields
  if (sysCode) {
    errorObject.IAPerror.code = sysCode;
  }
  if (sysRes) {
    errorObject.IAPerror.raw_response = sysRes;
  }
  if (stack) {
    errorObject.IAPerror.stack = stack;
  }

  // return the object
  return errorObject;
}

/**
 * This is the adapter/interface into Batfish
 */

/* GENERAL ADAPTER FUNCTIONS */
class Batfish extends EventEmitterCl {
  /**
   * Batfish Adapter
   * @constructor
   */
  constructor(prongid, properties) {
    super();

    this.props = properties;
    this.alive = false;
    this.healthy = false;
    this.id = prongid;
    this.healthcheckType = 'none';
    myid = prongid;

    workbench.init();
    this.clients = workbench.instantiateClients(`${this.props.host}:${this.props.port}`, (this.props.protocol === 'https' ? this.props.ssl.cert_file : ''));

    // TO-DO: might be nice to work out multiple authentication methods like in adapter-utils
    const auth = this.props.authentication;
    const hh = 'header.headers.';
    if (auth.auth_method !== 'no_authentication') {
      const tokenHeader = (auth.auth_field && auth.auth_field.startsWith(hh)) ? auth.auth_field.substring(hh.length) : 'Authorization';
      workbench.setCallTokenHeaderName(tokenHeader);
    }
    if (auth.auth_method === 'static_token') {
      workbench.updateCallToken(auth.token);
    }

    // get the path for the specific error file
    const errorFile = path.join(__dirname, '/error.json');

    // if the file does not exist - error
    if (!fs.existsSync(errorFile)) {
      const origin = `${this.id}-adapter-constructor`;
      log.warn(`${origin}: Could not locate ${errorFile} - errors will be missing details`);
    }

    // Read the action from the file system
    const errorData = JSON.parse(fs.readFileSync(errorFile, 'utf-8'));
    ({ errors } = errorData);

    // Instantiate the other components for this Adapter
    this.requestHandlerInst = new RequestHandlerCl(this.id, this.props, __dirname);
  }

  /**
   * @summary Connect function is used during Pronghorn startup to provide instantiation feedback.
   *
   * @function connect
   */
  connect() {
    const meth = 'adapter-connect';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    // initially set as off
    this.emit('OFFLINE', { id: this.id });
    this.alive = true;

    // if there is no healthcheck just change the emit to ONLINE
    // We do not recommend no healthcheck!!!
    if (this.healthcheckType === 'none') {
      log.error(`${origin}: Waiting 1 Seconds to emit Online`);
      setTimeout(() => {
        this.emit('ONLINE', { id: this.id });
        this.healthy = true;
      }, 1000);
    }

    // is the healthcheck only suppose to run on startup
    // (intermittent runs on startup and after that)
    if (this.healthcheckType === 'startup' || this.healthcheckType === 'intermittent') {
      // run an initial healthcheck
      this.healthCheck(null, (status) => {
        log.spam(`${origin}: ${status}`);
      });
    }

    // is the healthcheck suppose to run intermittently
    if (this.healthcheckType === 'intermittent') {
      // run the healthcheck in an interval
      setInterval(() => {
        // try to see if mongo is available
        this.healthCheck(null, (status) => {
          log.spam(`${origin}: ${status}`);
        });
      }, this.healthcheckInterval);
    }
  }

  /**
   * @callback healthCallback
   * @param {Object} reqObj - the request to send into the healthcheck
   * @param {Callback} callback - The results of the call
   */
  healthCheck(reqObj, callback) {
    // you can modify what is passed into the healthcheck by changing things in the newReq
    let newReq = null;
    if (reqObj) {
      newReq = Object.assign(...reqObj);
    }
    // need to work on this later
    this.healthy = true;
    this.emit('ONLINE', { id: this.id });
    const retstatus = { id: this.id, status: 'success' };
    return callback(retstatus);
  }

  /**
   * getAllFunctions is used to get all of the exposed function in the adapter
   *
   * @function getAllFunctions
   */
  getAllFunctions() {
    let myfunctions = [];
    let obj = this;

    // find the functions in this class
    do {
      const l = Object.getOwnPropertyNames(obj)
        .concat(Object.getOwnPropertySymbols(obj).map((s) => s.toString()))
        .sort()
        .filter((p, i, arr) => typeof obj[p] === 'function' && p !== 'constructor' && (i === 0 || p !== arr[i - 1]) && myfunctions.indexOf(p) === -1);
      myfunctions = myfunctions.concat(l);
    }
    while (
      (obj = Object.getPrototypeOf(obj)) && Object.getPrototypeOf(obj)
    );

    return myfunctions;
  }

  /**
   * getWorkflowFunctions is used to get all of the workflow function in the adapter
   *
   * @function getWorkflowFunctions
   */
  getWorkflowFunctions() {
    const myfunctions = this.getAllFunctions();
    const wffunctions = [];

    // remove the functions that should not be in a Workflow
    for (let m = 0; m < myfunctions.length; m += 1) {
      if (myfunctions[m] === 'addListener') {
        // got to the second tier (adapterBase)
        break;
      }
      if (myfunctions[m] !== 'connect' && myfunctions[m] !== 'healthCheck'
        && myfunctions[m] !== 'getAllFunctions' && myfunctions[m] !== 'getWorkflowFunctions'
        && myfunctions[m] !== 'checkProperties' && myfunctions[m] !== 'getToken') {
        wffunctions.push(myfunctions[m]);
      }
    }

    return wffunctions;
  }

  /**
   * checkProperties is used to validate the adapter properties.
   *
   * @function checkProperties
   * @param {Object} properties - an object containing all of the properties
   */
  checkProperties(properties) {
    const origin = `${this.myid}-adapterBase-checkProperties`;
    log.trace(origin);

    // validate the properties for the adapter
    try {
      // get the path for the specific action file
      const propertyFile = path.join(__dirname, 'propertiesSchema.json');

      // Read the action from the file system
      const propertySchema = JSON.parse(fs.readFileSync(propertyFile, 'utf-8'));

      // add any defaults to the data
      const combinedProps = this.props;

      // validate the entity against the schema
      const ajvInst = new AjvCl();
      const validate = ajvInst.compile(propertySchema);
      const result = validate(combinedProps);

      // if invalid properties throw an error
      if (!result) {
        // create the generic part of an error object
        const errorObj = {
          origin,
          type: 'Schema Validation Failure',
          vars: [validate.errors[0].message]
        };

        // log and throw the error
        log.trace(`${origin}: Schema validation failure ${validate.errors[0].message}`);
        throw new Error(JSON.stringify(errorObj));
      }

      // return the resulting properties --- add any necessary defaults
      return combinedProps;
    } catch (e) {
      return { exception: 'Exception increase log level' };
    }
  }

  /**
   * @summary get a token to be used in subsequent calls
   *
   * @function getToken
   *
   * @param {Callback} callback - A Token object
   */
  getToken(callback) {
    const origin = `${this.myid}-adapterBase-getToken`;
    log.trace(origin);

    if (!this.props.authentication || !this.props.authentication.username || !this.props.authentication.password) {
      const errorObj = formatErrorObject(origin, 'Unable To Authenticate', ['Token', 'Username and Password Required'], null, null, null);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }

    // get the token through the adapter-utils
    return this.requestHandlerInst.makeTokenRequest(null, null, (tokenObj, error) => {
      if (error) {
        return callback(null, error);
      }
      if (!tokenObj || !tokenObj.token) {
        const errorObj = formatErrorObject(origin, 'Unable To Authenticate', ['Token', 'No Token Returned'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      return callback(tokenObj.token);
    });
  }

  /**
   * @function parserParse
   * @pronghornType method
   * @name parserParse
   * @summary parserParse
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /parserParse
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  parserParse(body, callback) {
    const meth = 'adapter-parserParse';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.parser.ParseRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.Parser.Parse(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'parserParse failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.Parser.Parse(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'parserParse failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreateNetwork
   * @pronghornType method
   * @name apiGatewayCreateNetwork
   * @summary apiGatewayCreateNetwork
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreateNetwork
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreateNetwork(body, callback) {
    const meth = 'adapter-apiGatewayCreateNetwork';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreateNetworkRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreateNetwork(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreateNetwork failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreateNetwork(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreateNetwork failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteNetwork
   * @pronghornType method
   * @name apiGatewayDeleteNetwork
   * @summary apiGatewayDeleteNetwork
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteNetwork
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteNetwork(body, callback) {
    const meth = 'adapter-apiGatewayDeleteNetwork';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteNetworkRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteNetwork(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteNetwork failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteNetwork(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteNetwork failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetNetworkMetadata
   * @pronghornType method
   * @name apiGatewayGetNetworkMetadata
   * @summary apiGatewayGetNetworkMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetNetworkMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetNetworkMetadata(body, callback) {
    const meth = 'adapter-apiGatewayGetNetworkMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetNetworkMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetNetworkMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetNetworkMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetNetworkMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetNetworkMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListNetworkMetadata
   * @pronghornType method
   * @name apiGatewayListNetworkMetadata
   * @summary apiGatewayListNetworkMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListNetworkMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListNetworkMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListNetworkMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListNetworkMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListNetworkMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListNetworkMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListNetworkMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListNetworkMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetNetworkAggregates
   * @pronghornType method
   * @name apiGatewayGetNetworkAggregates
   * @summary apiGatewayGetNetworkAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetNetworkAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetNetworkAggregates(body, callback) {
    const meth = 'adapter-apiGatewayGetNetworkAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetNetworkAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetNetworkAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetNetworkAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetNetworkAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetNetworkAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutNetworkAggregates
   * @pronghornType method
   * @name apiGatewayPutNetworkAggregates
   * @summary apiGatewayPutNetworkAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutNetworkAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutNetworkAggregates(body, callback) {
    const meth = 'adapter-apiGatewayPutNetworkAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutNetworkAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutNetworkAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutNetworkAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutNetworkAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutNetworkAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteSnapshot
   * @pronghornType method
   * @name apiGatewayDeleteSnapshot
   * @summary apiGatewayDeleteSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteSnapshot(body, callback) {
    const meth = 'adapter-apiGatewayDeleteSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayForkSnapshot
   * @pronghornType method
   * @name apiGatewayForkSnapshot
   * @summary apiGatewayForkSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayForkSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayForkSnapshot(body, callback) {
    const meth = 'adapter-apiGatewayForkSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ForkSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ForkSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayForkSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ForkSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayForkSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayForkFromMasterSnapshot
   * @pronghornType method
   * @name apiGatewayForkFromMasterSnapshot
   * @summary apiGatewayForkFromMasterSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayForkFromMasterSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayForkFromMasterSnapshot(body, callback) {
    const meth = 'adapter-apiGatewayForkFromMasterSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ForkFromMasterSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ForkFromMasterSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayForkFromMasterSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ForkFromMasterSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayForkFromMasterSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotMetadata
   * @pronghornType method
   * @name apiGatewayGetSnapshotMetadata
   * @summary apiGatewayGetSnapshotMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotMetadata(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotStatus
   * @pronghornType method
   * @name apiGatewayGetSnapshotStatus
   * @summary apiGatewayGetSnapshotStatus
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotStatus
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotStatus(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotStatus';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotStatusRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotStatus(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotStatus failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotStatus(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotStatus failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotErrors
   * @pronghornType method
   * @name apiGatewayGetSnapshotErrors
   * @summary apiGatewayGetSnapshotErrors
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotErrors
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotErrors(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotErrors';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotErrorsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotErrors(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotErrors failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotErrors(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotErrors failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayInitSnapshot
   * @pronghornType method
   * @name apiGatewayInitSnapshot
   * @summary apiGatewayInitSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayInitSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayInitSnapshot(body, callback) {
    const meth = 'adapter-apiGatewayInitSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.InitSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.InitSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayInitSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.InitSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayInitSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListSnapshotMetadata
   * @pronghornType method
   * @name apiGatewayListSnapshotMetadata
   * @summary apiGatewayListSnapshotMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListSnapshotMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListSnapshotMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListSnapshotMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListSnapshotMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListSnapshotMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListSnapshotMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListSnapshots
   * @pronghornType method
   * @name apiGatewayListSnapshots
   * @summary apiGatewayListSnapshots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListSnapshots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListSnapshots(body, callback) {
    const meth = 'adapter-apiGatewayListSnapshots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListSnapshotsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListSnapshots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListSnapshots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListSnapshots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListSnapshots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetTopologySummary
   * @pronghornType method
   * @name apiGatewayGetTopologySummary
   * @summary apiGatewayGetTopologySummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetTopologySummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetTopologySummary(body, callback) {
    const meth = 'adapter-apiGatewayGetTopologySummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologySummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetTopologySummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologySummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetTopologySummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologySummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetTopologyLayout
   * @pronghornType method
   * @name apiGatewayGetTopologyLayout
   * @summary apiGatewayGetTopologyLayout
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetTopologyLayout
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetTopologyLayout(body, callback) {
    const meth = 'adapter-apiGatewayGetTopologyLayout';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyLayoutRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetTopologyLayout(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyLayout failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetTopologyLayout(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyLayout failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotInputObject
   * @pronghornType method
   * @name apiGatewayGetSnapshotInputObject
   * @summary apiGatewayGetSnapshotInputObject
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotInputObject
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotInputObject(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotInputObject';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotInputObjectRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotInputObject(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotInputObject failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotInputObject(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotInputObject failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayInitSnapshotComparison
   * @pronghornType method
   * @name apiGatewayInitSnapshotComparison
   * @summary apiGatewayInitSnapshotComparison
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayInitSnapshotComparison
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayInitSnapshotComparison(body, callback) {
    const meth = 'adapter-apiGatewayInitSnapshotComparison';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.InitSnapshotComparisonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.InitSnapshotComparison(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayInitSnapshotComparison failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.InitSnapshotComparison(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayInitSnapshotComparison failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonMetadata
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonMetadata
   * @summary apiGatewayGetSnapshotComparisonMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonMetadata(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonConfigurationsSummary
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonConfigurationsSummary
   * @summary apiGatewayGetSnapshotComparisonConfigurationsSummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonConfigurationsSummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonConfigurationsSummary(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonConfigurationsSummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonConfigurationsSummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonConfigurationsSummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonConfigurationsSummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonConfigurationsSummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonConfigurationsSummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonDevices
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonDevices
   * @summary apiGatewayGetSnapshotComparisonDevices
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonDevices
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonDevices(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonDevicesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonDevices(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonDevices failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonDevices(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonDevices failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonInterfaces
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonInterfaces
   * @summary apiGatewayGetSnapshotComparisonInterfaces
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonInterfaces
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonInterfaces(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonInterfacesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonInterfaces(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonInterfaces failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonInterfaces(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonInterfaces failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonReachability
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonReachability
   * @summary apiGatewayGetSnapshotComparisonReachability
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonReachability
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonReachability(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonReachability';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonReachabilityRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonReachability(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonReachability failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonReachability(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonReachability failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutesSummary
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutesSummary
   * @summary apiGatewayGetSnapshotComparisonRoutesSummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutesSummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutesSummary(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutesSummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutesSummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutesSummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutesSummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutesSummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutesSummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @summary apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @summary apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @summary apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfInterfaceRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfInterface(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfInterface(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @summary apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfNeighborRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfNeighbor(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfNeighbor(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @pronghornType method
   * @name apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @summary apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess(body, callback) {
    const meth = 'adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfProcessRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfProcess(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetSnapshotComparisonRoutingProtocolsOspfProcess(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreatePolicy
   * @pronghornType method
   * @name apiGatewayCreatePolicy
   * @summary apiGatewayCreatePolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreatePolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreatePolicy(body, callback) {
    const meth = 'adapter-apiGatewayCreatePolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreatePolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreatePolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreatePolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreatePolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreatePolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetPolicyResult
   * @pronghornType method
   * @name apiGatewayGetPolicyResult
   * @summary apiGatewayGetPolicyResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetPolicyResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetPolicyResult(body, callback) {
    const meth = 'adapter-apiGatewayGetPolicyResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetPolicyResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetPolicyResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetPolicyResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetPolicyResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetPolicyResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetPolicy
   * @pronghornType method
   * @name apiGatewayGetPolicy
   * @summary apiGatewayGetPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetPolicy(body, callback) {
    const meth = 'adapter-apiGatewayGetPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListPolicies
   * @pronghornType method
   * @name apiGatewayListPolicies
   * @summary apiGatewayListPolicies
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListPolicies
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListPolicies(body, callback) {
    const meth = 'adapter-apiGatewayListPolicies';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListPoliciesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListPolicies(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListPolicies failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListPolicies(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListPolicies failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListPolicyResultsMetadata
   * @pronghornType method
   * @name apiGatewayListPolicyResultsMetadata
   * @summary apiGatewayListPolicyResultsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListPolicyResultsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListPolicyResultsMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListPolicyResultsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListPolicyResultsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListPolicyResultsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListPolicyResultsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListPolicyResultsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListPolicyResultsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeletePolicy
   * @pronghornType method
   * @name apiGatewayDeletePolicy
   * @summary apiGatewayDeletePolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeletePolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeletePolicy(body, callback) {
    const meth = 'adapter-apiGatewayDeletePolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeletePolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeletePolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeletePolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeletePolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeletePolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreateAdHocAssertion
   * @pronghornType method
   * @name apiGatewayCreateAdHocAssertion
   * @summary apiGatewayCreateAdHocAssertion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreateAdHocAssertion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreateAdHocAssertion(body, callback) {
    const meth = 'adapter-apiGatewayCreateAdHocAssertion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreateAdHocAssertionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreateAdHocAssertion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreateAdHocAssertion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreateAdHocAssertion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreateAdHocAssertion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetAdHocAssertionResult
   * @pronghornType method
   * @name apiGatewayGetAdHocAssertionResult
   * @summary apiGatewayGetAdHocAssertionResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetAdHocAssertionResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetAdHocAssertionResult(body, callback) {
    const meth = 'adapter-apiGatewayGetAdHocAssertionResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAdHocAssertionResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetAdHocAssertionResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetAdHocAssertionResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetAdHocAssertionResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetAdHocAssertionResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetAdHocAssertion
   * @pronghornType method
   * @name apiGatewayGetAdHocAssertion
   * @summary apiGatewayGetAdHocAssertion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetAdHocAssertion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetAdHocAssertion(body, callback) {
    const meth = 'adapter-apiGatewayGetAdHocAssertion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAdHocAssertionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetAdHocAssertion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetAdHocAssertion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetAdHocAssertion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetAdHocAssertion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListAdHocAssertionMetadata
   * @pronghornType method
   * @name apiGatewayListAdHocAssertionMetadata
   * @summary apiGatewayListAdHocAssertionMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListAdHocAssertionMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListAdHocAssertionMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListAdHocAssertionMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListAdHocAssertionMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListAdHocAssertionMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListAdHocAssertionMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListAdHocAssertionMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListAdHocAssertionMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListInsights
   * @pronghornType method
   * @name apiGatewayListInsights
   * @summary apiGatewayListInsights
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListInsights
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListInsights(body, callback) {
    const meth = 'adapter-apiGatewayListInsights';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListInsightsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListInsights(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListInsights failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListInsights(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListInsights failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetInsight
   * @pronghornType method
   * @name apiGatewayGetInsight
   * @summary apiGatewayGetInsight
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetInsight
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetInsight(body, callback) {
    const meth = 'adapter-apiGatewayGetInsight';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetInsightRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetInsight(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetInsight failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetInsight(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetInsight failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListInsightResultsMetadata
   * @pronghornType method
   * @name apiGatewayListInsightResultsMetadata
   * @summary apiGatewayListInsightResultsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListInsightResultsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListInsightResultsMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListInsightResultsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListInsightResultsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListInsightResultsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListInsightResultsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListInsightResultsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListInsightResultsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetInsightResult
   * @pronghornType method
   * @name apiGatewayGetInsightResult
   * @summary apiGatewayGetInsightResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetInsightResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetInsightResult(body, callback) {
    const meth = 'adapter-apiGatewayGetInsightResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetInsightResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetInsightResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetInsightResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetInsightResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetInsightResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetAssertionInputJson
   * @pronghornType method
   * @name apiGatewayGetAssertionInputJson
   * @summary apiGatewayGetAssertionInputJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetAssertionInputJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetAssertionInputJson(body, callback) {
    const meth = 'adapter-apiGatewayGetAssertionInputJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAssertionInputJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetAssertionInputJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetAssertionInputJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetAssertionInputJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetAssertionInputJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayExportAssertionInput
   * @pronghornType method
   * @name apiGatewayExportAssertionInput
   * @summary apiGatewayExportAssertionInput
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayExportAssertionInput
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayExportAssertionInput(body, callback) {
    const meth = 'adapter-apiGatewayExportAssertionInput';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ExportAssertionInputRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ExportAssertionInput(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayExportAssertionInput failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ExportAssertionInput(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayExportAssertionInput failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetDebugSnapshots
   * @pronghornType method
   * @name apiGatewayGetDebugSnapshots
   * @summary apiGatewayGetDebugSnapshots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetDebugSnapshots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetDebugSnapshots(body, callback) {
    const meth = 'adapter-apiGatewayGetDebugSnapshots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetDebugSnapshotsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetDebugSnapshots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetDebugSnapshots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetDebugSnapshots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetDebugSnapshots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetDataRetentionPolicy
   * @pronghornType method
   * @name apiGatewayGetDataRetentionPolicy
   * @summary apiGatewayGetDataRetentionPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetDataRetentionPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetDataRetentionPolicy(body, callback) {
    const meth = 'adapter-apiGatewayGetDataRetentionPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetDataRetentionPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetDataRetentionPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetDataRetentionPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateDataRetentionPolicy
   * @pronghornType method
   * @name apiGatewayUpdateDataRetentionPolicy
   * @summary apiGatewayUpdateDataRetentionPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateDataRetentionPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateDataRetentionPolicy(body, callback) {
    const meth = 'adapter-apiGatewayUpdateDataRetentionPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateDataRetentionPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateDataRetentionPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateDataRetentionPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreateChangeReview
   * @pronghornType method
   * @name apiGatewayCreateChangeReview
   * @summary apiGatewayCreateChangeReview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreateChangeReview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreateChangeReview(body, callback) {
    const meth = 'adapter-apiGatewayCreateChangeReview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateChangeReviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreateChangeReview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreateChangeReview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreateChangeReview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreateChangeReview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayRunAllSteps
   * @pronghornType method
   * @name apiGatewayRunAllSteps
   * @summary apiGatewayRunAllSteps
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayRunAllSteps
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayRunAllSteps(body, callback) {
    const meth = 'adapter-apiGatewayRunAllSteps';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.RunAllStepsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.RunAllSteps(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayRunAllSteps failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.RunAllSteps(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayRunAllSteps failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetChangeReviewMetadata
   * @pronghornType method
   * @name apiGatewayGetChangeReviewMetadata
   * @summary apiGatewayGetChangeReviewMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetChangeReviewMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetChangeReviewMetadata(body, callback) {
    const meth = 'adapter-apiGatewayGetChangeReviewMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetChangeReviewMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetChangeReviewMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateChangeReview
   * @pronghornType method
   * @name apiGatewayUpdateChangeReview
   * @summary apiGatewayUpdateChangeReview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateChangeReview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateChangeReview(body, callback) {
    const meth = 'adapter-apiGatewayUpdateChangeReview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateChangeReviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateChangeReview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateChangeReview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateChangeReview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateChangeReview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListChangeReviewsMetadata
   * @pronghornType method
   * @name apiGatewayListChangeReviewsMetadata
   * @summary apiGatewayListChangeReviewsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListChangeReviewsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListChangeReviewsMetadata(body, callback) {
    const meth = 'adapter-apiGatewayListChangeReviewsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ListChangeReviewsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListChangeReviewsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListChangeReviewsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListChangeReviewsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListChangeReviewsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetChangeReviewStep
   * @pronghornType method
   * @name apiGatewayGetChangeReviewStep
   * @summary apiGatewayGetChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayGetChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetChangeReviewChange
   * @pronghornType method
   * @name apiGatewayGetChangeReviewChange
   * @summary apiGatewayGetChangeReviewChange
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetChangeReviewChange
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetChangeReviewChange(body, callback) {
    const meth = 'adapter-apiGatewayGetChangeReviewChange';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewChangeRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetChangeReviewChange(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewChange failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetChangeReviewChange(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewChange failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreateChangeReviewStep
   * @pronghornType method
   * @name apiGatewayCreateChangeReviewStep
   * @summary apiGatewayCreateChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreateChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreateChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayCreateChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreateChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreateChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateChangeReviewStep
   * @pronghornType method
   * @name apiGatewayUpdateChangeReviewStep
   * @summary apiGatewayUpdateChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayUpdateChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayRunChangeReviewStep
   * @pronghornType method
   * @name apiGatewayRunChangeReviewStep
   * @summary apiGatewayRunChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayRunChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayRunChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayRunChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.RunChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.RunChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayRunChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.RunChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayRunChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteChangeReviewStep
   * @pronghornType method
   * @name apiGatewayDeleteChangeReviewStep
   * @summary apiGatewayDeleteChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayDeleteChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.DeleteChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetChangeReviewStepJson
   * @pronghornType method
   * @name apiGatewayGetChangeReviewStepJson
   * @summary apiGatewayGetChangeReviewStepJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetChangeReviewStepJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetChangeReviewStepJson(body, callback) {
    const meth = 'adapter-apiGatewayGetChangeReviewStepJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetChangeReviewStepJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStepJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetChangeReviewStepJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStepJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayExportChangeReviewStep
   * @pronghornType method
   * @name apiGatewayExportChangeReviewStep
   * @summary apiGatewayExportChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayExportChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayExportChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayExportChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ExportChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ExportChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayExportChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ExportChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayExportChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetChangeReviewStepFromJson
   * @pronghornType method
   * @name apiGatewayGetChangeReviewStepFromJson
   * @summary apiGatewayGetChangeReviewStepFromJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetChangeReviewStepFromJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetChangeReviewStepFromJson(body, callback) {
    const meth = 'adapter-apiGatewayGetChangeReviewStepFromJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepFromJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetChangeReviewStepFromJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStepFromJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetChangeReviewStepFromJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetChangeReviewStepFromJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayParseChangeReviewStep
   * @pronghornType method
   * @name apiGatewayParseChangeReviewStep
   * @summary apiGatewayParseChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayParseChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayParseChangeReviewStep(body, callback) {
    const meth = 'adapter-apiGatewayParseChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ParseChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ParseChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayParseChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ParseChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayParseChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCreateStepPreview
   * @pronghornType method
   * @name apiGatewayCreateStepPreview
   * @summary apiGatewayCreateStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCreateStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCreateStepPreview(body, callback) {
    const meth = 'adapter-apiGatewayCreateStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CreateStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCreateStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CreateStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCreateStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateStepPreview
   * @pronghornType method
   * @name apiGatewayUpdateStepPreview
   * @summary apiGatewayUpdateStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateStepPreview(body, callback) {
    const meth = 'adapter-apiGatewayUpdateStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteStepPreview
   * @pronghornType method
   * @name apiGatewayDeleteStepPreview
   * @summary apiGatewayDeleteStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteStepPreview(body, callback) {
    const meth = 'adapter-apiGatewayDeleteStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.DeleteStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetStepPreviewResult
   * @pronghornType method
   * @name apiGatewayGetStepPreviewResult
   * @summary apiGatewayGetStepPreviewResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetStepPreviewResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetStepPreviewResult(body, callback) {
    const meth = 'adapter-apiGatewayGetStepPreviewResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetStepPreviewResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetStepPreviewResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetStepPreviewResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetStepPreviewResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetStepPreviewResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayCommitStepPreview
   * @pronghornType method
   * @name apiGatewayCommitStepPreview
   * @summary apiGatewayCommitStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayCommitStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayCommitStepPreview(body, callback) {
    const meth = 'adapter-apiGatewayCommitStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CommitStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.CommitStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayCommitStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.CommitStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayCommitStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayActivateLicense
   * @pronghornType method
   * @name apiGatewayActivateLicense
   * @summary apiGatewayActivateLicense
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayActivateLicense
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayActivateLicense(body, callback) {
    const meth = 'adapter-apiGatewayActivateLicense';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ActivateLicenseRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ActivateLicense(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayActivateLicense failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ActivateLicense(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayActivateLicense failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetLicenseUser
   * @pronghornType method
   * @name apiGatewayGetLicenseUser
   * @summary apiGatewayGetLicenseUser
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetLicenseUser
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetLicenseUser(body, callback) {
    const meth = 'adapter-apiGatewayGetLicenseUser';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetLicenseUserRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetLicenseUser(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetLicenseUser failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetLicenseUser(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetLicenseUser failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}\n${ex}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetLicenseMetadata
   * @pronghornType method
   * @name apiGatewayGetLicenseMetadata
   * @summary apiGatewayGetLicenseMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetLicenseMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetLicenseMetadata(body, callback) {
    const meth = 'adapter-apiGatewayGetLicenseMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetLicenseMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetLicenseMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetLicenseMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetLicenseMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetLicenseMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetCloudFetchSettings
   * @pronghornType method
   * @name apiGatewayGetCloudFetchSettings
   * @summary apiGatewayGetCloudFetchSettings
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetCloudFetchSettings
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetCloudFetchSettings(body, callback) {
    const meth = 'adapter-apiGatewayGetCloudFetchSettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetCloudFetchSettingsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetCloudFetchSettings failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetCloudFetchSettings failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateCloudFetchSettings
   * @pronghornType method
   * @name apiGatewayUpdateCloudFetchSettings
   * @summary apiGatewayUpdateCloudFetchSettings
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateCloudFetchSettings
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateCloudFetchSettings(body, callback) {
    const meth = 'adapter-apiGatewayUpdateCloudFetchSettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateCloudFetchSettingsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateCloudFetchSettings failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateCloudFetchSettings failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetAwsAccountsStatuses
   * @pronghornType method
   * @name apiGatewayGetAwsAccountsStatuses
   * @summary apiGatewayGetAwsAccountsStatuses
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetAwsAccountsStatuses
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetAwsAccountsStatuses(body, callback) {
    const meth = 'adapter-apiGatewayGetAwsAccountsStatuses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAwsAccountsStatusesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetAwsAccountsStatuses(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetAwsAccountsStatuses failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetAwsAccountsStatuses(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetAwsAccountsStatuses failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetCloudFetchStatus
   * @pronghornType method
   * @name apiGatewayGetCloudFetchStatus
   * @summary apiGatewayGetCloudFetchStatus
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetCloudFetchStatus
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetCloudFetchStatus(body, callback) {
    const meth = 'adapter-apiGatewayGetCloudFetchStatus';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetCloudFetchStatusRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetCloudFetchStatus(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetCloudFetchStatus failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetCloudFetchStatus(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetCloudFetchStatus failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayAddAwsAccount
   * @pronghornType method
   * @name apiGatewayAddAwsAccount
   * @summary apiGatewayAddAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayAddAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayAddAwsAccount(body, callback) {
    const meth = 'adapter-apiGatewayAddAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.AddAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.AddAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayAddAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.AddAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayAddAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateAwsAccount
   * @pronghornType method
   * @name apiGatewayUpdateAwsAccount
   * @summary apiGatewayUpdateAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateAwsAccount(body, callback) {
    const meth = 'adapter-apiGatewayUpdateAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteAwsAccount
   * @pronghornType method
   * @name apiGatewayDeleteAwsAccount
   * @summary apiGatewayDeleteAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteAwsAccount(body, callback) {
    const meth = 'adapter-apiGatewayDeleteAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayUpdateCloudFetchPollInterval
   * @pronghornType method
   * @name apiGatewayUpdateCloudFetchPollInterval
   * @summary apiGatewayUpdateCloudFetchPollInterval
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayUpdateCloudFetchPollInterval
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayUpdateCloudFetchPollInterval(body, callback) {
    const meth = 'adapter-apiGatewayUpdateCloudFetchPollInterval';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateCloudFetchPollIntervalRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.UpdateCloudFetchPollInterval(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayUpdateCloudFetchPollInterval failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.UpdateCloudFetchPollInterval(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayUpdateCloudFetchPollInterval failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayParse
   * @pronghornType method
   * @name apiGatewayParse
   * @summary apiGatewayParse
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayParse
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayParse(body, callback) {
    const meth = 'adapter-apiGatewayParse';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.parser.ParseRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.Parse(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayParse failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.Parse(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayParse failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetQuestion
   * @pronghornType method
   * @name apiGatewayGetQuestion
   * @summary apiGatewayGetQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetQuestion(body, callback) {
    const meth = 'adapter-apiGatewayGetQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutQuestion
   * @pronghornType method
   * @name apiGatewayPutQuestion
   * @summary apiGatewayPutQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutQuestion(body, callback) {
    const meth = 'adapter-apiGatewayPutQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteQuestion
   * @pronghornType method
   * @name apiGatewayDeleteQuestion
   * @summary apiGatewayDeleteQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteQuestion(body, callback) {
    const meth = 'adapter-apiGatewayDeleteQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetQuestions
   * @pronghornType method
   * @name apiGatewayGetQuestions
   * @summary apiGatewayGetQuestions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetQuestions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetQuestions(body, callback) {
    const meth = 'adapter-apiGatewayGetQuestions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetQuestionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetQuestions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetQuestions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetQuestions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetQuestions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayListQuestions
   * @pronghornType method
   * @name apiGatewayListQuestions
   * @summary apiGatewayListQuestions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayListQuestions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayListQuestions(body, callback) {
    const meth = 'adapter-apiGatewayListQuestions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListQuestionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.ListQuestions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayListQuestions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.ListQuestions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayListQuestions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetReferenceBook
   * @pronghornType method
   * @name apiGatewayGetReferenceBook
   * @summary apiGatewayGetReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetReferenceBook(body, callback) {
    const meth = 'adapter-apiGatewayGetReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteReferenceBook
   * @pronghornType method
   * @name apiGatewayDeleteReferenceBook
   * @summary apiGatewayDeleteReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteReferenceBook(body, callback) {
    const meth = 'adapter-apiGatewayDeleteReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutReferenceBook
   * @pronghornType method
   * @name apiGatewayPutReferenceBook
   * @summary apiGatewayPutReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutReferenceBook(body, callback) {
    const meth = 'adapter-apiGatewayPutReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetReferenceLibrary
   * @pronghornType method
   * @name apiGatewayGetReferenceLibrary
   * @summary apiGatewayGetReferenceLibrary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetReferenceLibrary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetReferenceLibrary(body, callback) {
    const meth = 'adapter-apiGatewayGetReferenceLibrary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetReferenceLibraryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetReferenceLibrary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetReferenceLibrary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetReferenceLibrary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetReferenceLibrary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetTopologyAggregates
   * @pronghornType method
   * @name apiGatewayGetTopologyAggregates
   * @summary apiGatewayGetTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetTopologyAggregates(body, callback) {
    const meth = 'adapter-apiGatewayGetTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteTopologyAggregates
   * @pronghornType method
   * @name apiGatewayDeleteTopologyAggregates
   * @summary apiGatewayDeleteTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteTopologyAggregates(body, callback) {
    const meth = 'adapter-apiGatewayDeleteTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutTopologyAggregates
   * @pronghornType method
   * @name apiGatewayPutTopologyAggregates
   * @summary apiGatewayPutTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutTopologyAggregates(body, callback) {
    const meth = 'adapter-apiGatewayPutTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetTopologyPositions
   * @pronghornType method
   * @name apiGatewayGetTopologyPositions
   * @summary apiGatewayGetTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetTopologyPositions(body, callback) {
    const meth = 'adapter-apiGatewayGetTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteTopologyPositions
   * @pronghornType method
   * @name apiGatewayDeleteTopologyPositions
   * @summary apiGatewayDeleteTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteTopologyPositions(body, callback) {
    const meth = 'adapter-apiGatewayDeleteTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutTopologyPositions
   * @pronghornType method
   * @name apiGatewayPutTopologyPositions
   * @summary apiGatewayPutTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutTopologyPositions(body, callback) {
    const meth = 'adapter-apiGatewayPutTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetTopologyRoots
   * @pronghornType method
   * @name apiGatewayGetTopologyRoots
   * @summary apiGatewayGetTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetTopologyRoots(body, callback) {
    const meth = 'adapter-apiGatewayGetTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayDeleteTopologyRoots
   * @pronghornType method
   * @name apiGatewayDeleteTopologyRoots
   * @summary apiGatewayDeleteTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayDeleteTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayDeleteTopologyRoots(body, callback) {
    const meth = 'adapter-apiGatewayDeleteTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.DeleteTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.DeleteTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayDeleteTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayPutTopologyRoots
   * @pronghornType method
   * @name apiGatewayPutTopologyRoots
   * @summary apiGatewayPutTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayPutTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayPutTopologyRoots(body, callback) {
    const meth = 'adapter-apiGatewayPutTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.PutTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.PutTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayPutTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayWorkMgrV1
   * @pronghornType method
   * @name apiGatewayWorkMgrV1
   * @summary apiGatewayWorkMgrV1
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayWorkMgrV1
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayWorkMgrV1(body, callback) {
    const meth = 'adapter-apiGatewayWorkMgrV1';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.WorkMgrV1Request';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.WorkMgrV1(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayWorkMgrV1 failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.WorkMgrV1(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayWorkMgrV1 failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayWorkMgrV2
   * @pronghornType method
   * @name apiGatewayWorkMgrV2
   * @summary apiGatewayWorkMgrV2
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayWorkMgrV2
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayWorkMgrV2(body, callback) {
    const meth = 'adapter-apiGatewayWorkMgrV2';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.WorkMgrV2Request';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.WorkMgrV2(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayWorkMgrV2 failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.WorkMgrV2(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayWorkMgrV2 failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function apiGatewayGetWebAccessToken
   * @pronghornType method
   * @name apiGatewayGetWebAccessToken
   * @summary apiGatewayGetWebAccessToken
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /apiGatewayGetWebAccessToken
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  apiGatewayGetWebAccessToken(body, callback) {
    const meth = 'adapter-apiGatewayGetWebAccessToken';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.auth.GetWebAccessTokenRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ApiGateway.GetWebAccessToken(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'apiGatewayGetWebAccessToken failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ApiGateway.GetWebAccessToken(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'apiGatewayGetWebAccessToken failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceCreateNetwork
   * @pronghornType method
   * @name snapshotServiceCreateNetwork
   * @summary snapshotServiceCreateNetwork
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceCreateNetwork
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceCreateNetwork(body, callback) {
    const meth = 'adapter-snapshotServiceCreateNetwork';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreateNetworkRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.CreateNetwork(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceCreateNetwork failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.CreateNetwork(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceCreateNetwork failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteNetwork
   * @pronghornType method
   * @name snapshotServiceDeleteNetwork
   * @summary snapshotServiceDeleteNetwork
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteNetwork
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteNetwork(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteNetwork';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteNetworkRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteNetwork(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteNetwork failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteNetwork(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteNetwork failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetNetworkMetadata
   * @pronghornType method
   * @name snapshotServiceGetNetworkMetadata
   * @summary snapshotServiceGetNetworkMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetNetworkMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetNetworkMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceGetNetworkMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetNetworkMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetNetworkMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetNetworkMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListNetworkMetadata
   * @pronghornType method
   * @name snapshotServiceListNetworkMetadata
   * @summary snapshotServiceListNetworkMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListNetworkMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListNetworkMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceListNetworkMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListNetworkMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListNetworkMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListNetworkMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListNetworkMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListNetworkMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetNetworkAggregates
   * @pronghornType method
   * @name snapshotServiceGetNetworkAggregates
   * @summary snapshotServiceGetNetworkAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetNetworkAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetNetworkAggregates(body, callback) {
    const meth = 'adapter-snapshotServiceGetNetworkAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetNetworkAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetNetworkAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetNetworkAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutNetworkAggregates
   * @pronghornType method
   * @name snapshotServicePutNetworkAggregates
   * @summary snapshotServicePutNetworkAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutNetworkAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutNetworkAggregates(body, callback) {
    const meth = 'adapter-snapshotServicePutNetworkAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutNetworkAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutNetworkAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutNetworkAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutNetworkAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutNetworkAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetQuestion
   * @pronghornType method
   * @name snapshotServiceGetQuestion
   * @summary snapshotServiceGetQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetQuestion(body, callback) {
    const meth = 'adapter-snapshotServiceGetQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutQuestion
   * @pronghornType method
   * @name snapshotServicePutQuestion
   * @summary snapshotServicePutQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutQuestion(body, callback) {
    const meth = 'adapter-snapshotServicePutQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteQuestion
   * @pronghornType method
   * @name snapshotServiceDeleteQuestion
   * @summary snapshotServiceDeleteQuestion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteQuestion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteQuestion(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteQuestion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteQuestionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteQuestion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteQuestion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteQuestion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteQuestion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetQuestions
   * @pronghornType method
   * @name snapshotServiceGetQuestions
   * @summary snapshotServiceGetQuestions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetQuestions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetQuestions(body, callback) {
    const meth = 'adapter-snapshotServiceGetQuestions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetQuestionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetQuestions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetQuestions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetQuestions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetQuestions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListQuestions
   * @pronghornType method
   * @name snapshotServiceListQuestions
   * @summary snapshotServiceListQuestions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListQuestions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListQuestions(body, callback) {
    const meth = 'adapter-snapshotServiceListQuestions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListQuestionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListQuestions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListQuestions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListQuestions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListQuestions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetReferenceBook
   * @pronghornType method
   * @name snapshotServiceGetReferenceBook
   * @summary snapshotServiceGetReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetReferenceBook(body, callback) {
    const meth = 'adapter-snapshotServiceGetReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteReferenceBook
   * @pronghornType method
   * @name snapshotServiceDeleteReferenceBook
   * @summary snapshotServiceDeleteReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteReferenceBook(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutReferenceBook
   * @pronghornType method
   * @name snapshotServicePutReferenceBook
   * @summary snapshotServicePutReferenceBook
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutReferenceBook
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutReferenceBook(body, callback) {
    const meth = 'adapter-snapshotServicePutReferenceBook';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutReferenceBookRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutReferenceBook(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutReferenceBook failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutReferenceBook(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutReferenceBook failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetReferenceLibrary
   * @pronghornType method
   * @name snapshotServiceGetReferenceLibrary
   * @summary snapshotServiceGetReferenceLibrary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetReferenceLibrary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetReferenceLibrary(body, callback) {
    const meth = 'adapter-snapshotServiceGetReferenceLibrary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetReferenceLibraryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetReferenceLibrary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetReferenceLibrary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetReferenceLibrary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetReferenceLibrary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetTopologyAggregates
   * @pronghornType method
   * @name snapshotServiceGetTopologyAggregates
   * @summary snapshotServiceGetTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetTopologyAggregates(body, callback) {
    const meth = 'adapter-snapshotServiceGetTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteTopologyAggregates
   * @pronghornType method
   * @name snapshotServiceDeleteTopologyAggregates
   * @summary snapshotServiceDeleteTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteTopologyAggregates(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutTopologyAggregates
   * @pronghornType method
   * @name snapshotServicePutTopologyAggregates
   * @summary snapshotServicePutTopologyAggregates
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutTopologyAggregates
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutTopologyAggregates(body, callback) {
    const meth = 'adapter-snapshotServicePutTopologyAggregates';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyAggregatesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutTopologyAggregates(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyAggregates failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutTopologyAggregates(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyAggregates failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetTopologyPositions
   * @pronghornType method
   * @name snapshotServiceGetTopologyPositions
   * @summary snapshotServiceGetTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetTopologyPositions(body, callback) {
    const meth = 'adapter-snapshotServiceGetTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteTopologyPositions
   * @pronghornType method
   * @name snapshotServiceDeleteTopologyPositions
   * @summary snapshotServiceDeleteTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteTopologyPositions(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutTopologyPositions
   * @pronghornType method
   * @name snapshotServicePutTopologyPositions
   * @summary snapshotServicePutTopologyPositions
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutTopologyPositions
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutTopologyPositions(body, callback) {
    const meth = 'adapter-snapshotServicePutTopologyPositions';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyPositionsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutTopologyPositions(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyPositions failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutTopologyPositions(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyPositions failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetTopologyRoots
   * @pronghornType method
   * @name snapshotServiceGetTopologyRoots
   * @summary snapshotServiceGetTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetTopologyRoots(body, callback) {
    const meth = 'adapter-snapshotServiceGetTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteTopologyRoots
   * @pronghornType method
   * @name snapshotServiceDeleteTopologyRoots
   * @summary snapshotServiceDeleteTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteTopologyRoots(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServicePutTopologyRoots
   * @pronghornType method
   * @name snapshotServicePutTopologyRoots
   * @summary snapshotServicePutTopologyRoots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServicePutTopologyRoots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServicePutTopologyRoots(body, callback) {
    const meth = 'adapter-snapshotServicePutTopologyRoots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.PutTopologyRootsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.PutTopologyRoots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyRoots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.PutTopologyRoots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServicePutTopologyRoots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeleteSnapshot
   * @pronghornType method
   * @name snapshotServiceDeleteSnapshot
   * @summary snapshotServiceDeleteSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeleteSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeleteSnapshot(body, callback) {
    const meth = 'adapter-snapshotServiceDeleteSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeleteSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeleteSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeleteSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceForkSnapshot
   * @pronghornType method
   * @name snapshotServiceForkSnapshot
   * @summary snapshotServiceForkSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceForkSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceForkSnapshot(body, callback) {
    const meth = 'adapter-snapshotServiceForkSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ForkSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ForkSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceForkSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ForkSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceForkSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceForkFromMasterSnapshot
   * @pronghornType method
   * @name snapshotServiceForkFromMasterSnapshot
   * @summary snapshotServiceForkFromMasterSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceForkFromMasterSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceForkFromMasterSnapshot(body, callback) {
    const meth = 'adapter-snapshotServiceForkFromMasterSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ForkFromMasterSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ForkFromMasterSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceForkFromMasterSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ForkFromMasterSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceForkFromMasterSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotMetadata
   * @pronghornType method
   * @name snapshotServiceGetSnapshotMetadata
   * @summary snapshotServiceGetSnapshotMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotStatus
   * @pronghornType method
   * @name snapshotServiceGetSnapshotStatus
   * @summary snapshotServiceGetSnapshotStatus
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotStatus
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotStatus(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotStatus';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotStatusRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotStatus(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotStatus failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotStatus(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotStatus failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotErrors
   * @pronghornType method
   * @name snapshotServiceGetSnapshotErrors
   * @summary snapshotServiceGetSnapshotErrors
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotErrors
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotErrors(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotErrors';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotErrorsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotErrors(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotErrors failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotErrors(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotErrors failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceInitSnapshot
   * @pronghornType method
   * @name snapshotServiceInitSnapshot
   * @summary snapshotServiceInitSnapshot
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceInitSnapshot
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceInitSnapshot(body, callback) {
    const meth = 'adapter-snapshotServiceInitSnapshot';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.InitSnapshotRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.InitSnapshot(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceInitSnapshot failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.InitSnapshot(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceInitSnapshot failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListSnapshotMetadata
   * @pronghornType method
   * @name snapshotServiceListSnapshotMetadata
   * @summary snapshotServiceListSnapshotMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListSnapshotMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListSnapshotMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceListSnapshotMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListSnapshotMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListSnapshotMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListSnapshotMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListSnapshotMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListSnapshots
   * @pronghornType method
   * @name snapshotServiceListSnapshots
   * @summary snapshotServiceListSnapshots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListSnapshots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListSnapshots(body, callback) {
    const meth = 'adapter-snapshotServiceListSnapshots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListSnapshotsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListSnapshots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListSnapshots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListSnapshots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListSnapshots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetTopologySummary
   * @pronghornType method
   * @name snapshotServiceGetTopologySummary
   * @summary snapshotServiceGetTopologySummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetTopologySummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetTopologySummary(body, callback) {
    const meth = 'adapter-snapshotServiceGetTopologySummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologySummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetTopologySummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologySummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetTopologySummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologySummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetTopologyLayout
   * @pronghornType method
   * @name snapshotServiceGetTopologyLayout
   * @summary snapshotServiceGetTopologyLayout
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetTopologyLayout
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetTopologyLayout(body, callback) {
    const meth = 'adapter-snapshotServiceGetTopologyLayout';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetTopologyLayoutRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetTopologyLayout(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyLayout failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetTopologyLayout(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetTopologyLayout failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotInputObject
   * @pronghornType method
   * @name snapshotServiceGetSnapshotInputObject
   * @summary snapshotServiceGetSnapshotInputObject
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotInputObject
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotInputObject(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotInputObject';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotInputObjectRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotInputObject(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotInputObject failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotInputObject(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotInputObject failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceInitSnapshotComparison
   * @pronghornType method
   * @name snapshotServiceInitSnapshotComparison
   * @summary snapshotServiceInitSnapshotComparison
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceInitSnapshotComparison
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceInitSnapshotComparison(body, callback) {
    const meth = 'adapter-snapshotServiceInitSnapshotComparison';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.InitSnapshotComparisonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.InitSnapshotComparison(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceInitSnapshotComparison failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.InitSnapshotComparison(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceInitSnapshotComparison failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonMetadata
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonMetadata
   * @summary snapshotServiceGetSnapshotComparisonMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonConfigurationsSummary
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonConfigurationsSummary
   * @summary snapshotServiceGetSnapshotComparisonConfigurationsSummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonConfigurationsSummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonConfigurationsSummary(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonConfigurationsSummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonConfigurationsSummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonConfigurationsSummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonConfigurationsSummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonConfigurationsSummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonConfigurationsSummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonDevices
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonDevices
   * @summary snapshotServiceGetSnapshotComparisonDevices
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonDevices
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonDevices(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonDevices';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonDevicesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonDevices(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonDevices failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonDevices(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonDevices failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonInterfaces
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonInterfaces
   * @summary snapshotServiceGetSnapshotComparisonInterfaces
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonInterfaces
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonInterfaces(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonInterfaces';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonInterfacesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonInterfaces(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonInterfaces failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonInterfaces(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonInterfaces failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonReachability
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonReachability
   * @summary snapshotServiceGetSnapshotComparisonReachability
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonReachability
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonReachability(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonReachability';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonReachabilityRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonReachability(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonReachability failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonReachability(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonReachability failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutesSummary
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutesSummary
   * @summary snapshotServiceGetSnapshotComparisonRoutesSummary
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutesSummary
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutesSummary(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutesSummary';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutesSummaryRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutesSummary(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutesSummary failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutesSummary(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutesSummary failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @summary snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @summary snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @summary snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfInterfaceRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfInterface(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfInterface(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @summary snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfNeighborRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfNeighbor(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfNeighbor(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @pronghornType method
   * @name snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @summary snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotComparisonRoutingProtocolsOspfProcessRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfProcess(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotComparisonRoutingProtocolsOspfProcess(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceCreatePolicy
   * @pronghornType method
   * @name snapshotServiceCreatePolicy
   * @summary snapshotServiceCreatePolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceCreatePolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceCreatePolicy(body, callback) {
    const meth = 'adapter-snapshotServiceCreatePolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreatePolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.CreatePolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceCreatePolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.CreatePolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceCreatePolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetPolicyResult
   * @pronghornType method
   * @name snapshotServiceGetPolicyResult
   * @summary snapshotServiceGetPolicyResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetPolicyResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetPolicyResult(body, callback) {
    const meth = 'adapter-snapshotServiceGetPolicyResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetPolicyResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetPolicyResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetPolicyResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetPolicyResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetPolicyResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetPolicy
   * @pronghornType method
   * @name snapshotServiceGetPolicy
   * @summary snapshotServiceGetPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetPolicy(body, callback) {
    const meth = 'adapter-snapshotServiceGetPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListPolicies
   * @pronghornType method
   * @name snapshotServiceListPolicies
   * @summary snapshotServiceListPolicies
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListPolicies
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListPolicies(body, callback) {
    const meth = 'adapter-snapshotServiceListPolicies';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListPoliciesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListPolicies(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListPolicies failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListPolicies(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListPolicies failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListPolicyResultsMetadata
   * @pronghornType method
   * @name snapshotServiceListPolicyResultsMetadata
   * @summary snapshotServiceListPolicyResultsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListPolicyResultsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListPolicyResultsMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceListPolicyResultsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListPolicyResultsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListPolicyResultsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListPolicyResultsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListPolicyResultsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListPolicyResultsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceDeletePolicy
   * @pronghornType method
   * @name snapshotServiceDeletePolicy
   * @summary snapshotServiceDeletePolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceDeletePolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceDeletePolicy(body, callback) {
    const meth = 'adapter-snapshotServiceDeletePolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeletePolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.DeletePolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceDeletePolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.DeletePolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceDeletePolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceCreateAdHocAssertion
   * @pronghornType method
   * @name snapshotServiceCreateAdHocAssertion
   * @summary snapshotServiceCreateAdHocAssertion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceCreateAdHocAssertion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceCreateAdHocAssertion(body, callback) {
    const meth = 'adapter-snapshotServiceCreateAdHocAssertion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.CreateAdHocAssertionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.CreateAdHocAssertion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceCreateAdHocAssertion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.CreateAdHocAssertion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceCreateAdHocAssertion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetAdHocAssertionResult
   * @pronghornType method
   * @name snapshotServiceGetAdHocAssertionResult
   * @summary snapshotServiceGetAdHocAssertionResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetAdHocAssertionResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetAdHocAssertionResult(body, callback) {
    const meth = 'adapter-snapshotServiceGetAdHocAssertionResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAdHocAssertionResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetAdHocAssertionResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetAdHocAssertionResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetAdHocAssertionResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetAdHocAssertionResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetAdHocAssertion
   * @pronghornType method
   * @name snapshotServiceGetAdHocAssertion
   * @summary snapshotServiceGetAdHocAssertion
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetAdHocAssertion
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetAdHocAssertion(body, callback) {
    const meth = 'adapter-snapshotServiceGetAdHocAssertion';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAdHocAssertionRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetAdHocAssertion(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetAdHocAssertion failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetAdHocAssertion(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetAdHocAssertion failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListAdHocAssertionMetadata
   * @pronghornType method
   * @name snapshotServiceListAdHocAssertionMetadata
   * @summary snapshotServiceListAdHocAssertionMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListAdHocAssertionMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListAdHocAssertionMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceListAdHocAssertionMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListAdHocAssertionMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListAdHocAssertionMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListAdHocAssertionMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListAdHocAssertionMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListAdHocAssertionMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListInsights
   * @pronghornType method
   * @name snapshotServiceListInsights
   * @summary snapshotServiceListInsights
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListInsights
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListInsights(body, callback) {
    const meth = 'adapter-snapshotServiceListInsights';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListInsightsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListInsights(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListInsights failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListInsights(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListInsights failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetInsight
   * @pronghornType method
   * @name snapshotServiceGetInsight
   * @summary snapshotServiceGetInsight
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetInsight
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetInsight(body, callback) {
    const meth = 'adapter-snapshotServiceGetInsight';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetInsightRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetInsight(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetInsight failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetInsight(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetInsight failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceListInsightResultsMetadata
   * @pronghornType method
   * @name snapshotServiceListInsightResultsMetadata
   * @summary snapshotServiceListInsightResultsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceListInsightResultsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceListInsightResultsMetadata(body, callback) {
    const meth = 'adapter-snapshotServiceListInsightResultsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ListInsightResultsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ListInsightResultsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceListInsightResultsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ListInsightResultsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceListInsightResultsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetInsightResult
   * @pronghornType method
   * @name snapshotServiceGetInsightResult
   * @summary snapshotServiceGetInsightResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetInsightResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetInsightResult(body, callback) {
    const meth = 'adapter-snapshotServiceGetInsightResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetInsightResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetInsightResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetInsightResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetInsightResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetInsightResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetAssertionInputJson
   * @pronghornType method
   * @name snapshotServiceGetAssertionInputJson
   * @summary snapshotServiceGetAssertionInputJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetAssertionInputJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetAssertionInputJson(body, callback) {
    const meth = 'adapter-snapshotServiceGetAssertionInputJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAssertionInputJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetAssertionInputJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetAssertionInputJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetAssertionInputJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetAssertionInputJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceExportAssertionInput
   * @pronghornType method
   * @name snapshotServiceExportAssertionInput
   * @summary snapshotServiceExportAssertionInput
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceExportAssertionInput
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceExportAssertionInput(body, callback) {
    const meth = 'adapter-snapshotServiceExportAssertionInput';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ExportAssertionInputRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ExportAssertionInput(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceExportAssertionInput failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ExportAssertionInput(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceExportAssertionInput failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetDebugSnapshots
   * @pronghornType method
   * @name snapshotServiceGetDebugSnapshots
   * @summary snapshotServiceGetDebugSnapshots
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetDebugSnapshots
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetDebugSnapshots(body, callback) {
    const meth = 'adapter-snapshotServiceGetDebugSnapshots';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetDebugSnapshotsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetDebugSnapshots(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetDebugSnapshots failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetDebugSnapshots(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetDebugSnapshots failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetDataRetentionPolicy
   * @pronghornType method
   * @name snapshotServiceGetDataRetentionPolicy
   * @summary snapshotServiceGetDataRetentionPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetDataRetentionPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetDataRetentionPolicy(body, callback) {
    const meth = 'adapter-snapshotServiceGetDataRetentionPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetDataRetentionPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetDataRetentionPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetDataRetentionPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceUpdateDataRetentionPolicy
   * @pronghornType method
   * @name snapshotServiceUpdateDataRetentionPolicy
   * @summary snapshotServiceUpdateDataRetentionPolicy
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceUpdateDataRetentionPolicy
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceUpdateDataRetentionPolicy(body, callback) {
    const meth = 'adapter-snapshotServiceUpdateDataRetentionPolicy';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateDataRetentionPolicyRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.UpdateDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceUpdateDataRetentionPolicy failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.UpdateDataRetentionPolicy(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceUpdateDataRetentionPolicy failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetNetworkId
   * @pronghornType method
   * @name snapshotServiceGetNetworkId
   * @summary snapshotServiceGetNetworkId
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetNetworkId
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetNetworkId(body, callback) {
    const meth = 'adapter-snapshotServiceGetNetworkId';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetNetworkIdRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetNetworkId(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkId failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetNetworkId(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetNetworkId failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceGetSnapshotId
   * @pronghornType method
   * @name snapshotServiceGetSnapshotId
   * @summary snapshotServiceGetSnapshotId
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceGetSnapshotId
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceGetSnapshotId(body, callback) {
    const meth = 'adapter-snapshotServiceGetSnapshotId';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetSnapshotIdRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.GetSnapshotId(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotId failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.GetSnapshotId(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceGetSnapshotId failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function snapshotServiceForkSnapshotWithChange
   * @pronghornType method
   * @name snapshotServiceForkSnapshotWithChange
   * @summary snapshotServiceForkSnapshotWithChange
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /snapshotServiceForkSnapshotWithChange
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  snapshotServiceForkSnapshotWithChange(body, callback) {
    const meth = 'adapter-snapshotServiceForkSnapshotWithChange';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ForkSnapshotWithChangeRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.SnapshotService.ForkSnapshotWithChange(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'snapshotServiceForkSnapshotWithChange failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.SnapshotService.ForkSnapshotWithChange(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'snapshotServiceForkSnapshotWithChange failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function licenseActivateLicense
   * @pronghornType method
   * @name licenseActivateLicense
   * @summary licenseActivateLicense
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /licenseActivateLicense
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  licenseActivateLicense(body, callback) {
    const meth = 'adapter-licenseActivateLicense';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.ActivateLicenseRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.License.ActivateLicense(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'licenseActivateLicense failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.License.ActivateLicense(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'licenseActivateLicense failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function licenseGetLicenseUser
   * @pronghornType method
   * @name licenseGetLicenseUser
   * @summary licenseGetLicenseUser
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /licenseGetLicenseUser
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  licenseGetLicenseUser(body, callback) {
    const meth = 'adapter-licenseGetLicenseUser';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetLicenseUserRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.License.GetLicenseUser(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'licenseGetLicenseUser failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.License.GetLicenseUser(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'licenseGetLicenseUser failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function licenseGetLicenseMetadata
   * @pronghornType method
   * @name licenseGetLicenseMetadata
   * @summary licenseGetLicenseMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /licenseGetLicenseMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  licenseGetLicenseMetadata(body, callback) {
    const meth = 'adapter-licenseGetLicenseMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetLicenseMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.License.GetLicenseMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'licenseGetLicenseMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.License.GetLicenseMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'licenseGetLicenseMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchGetCloudFetchSettings
   * @pronghornType method
   * @name cloudFetchGetCloudFetchSettings
   * @summary cloudFetchGetCloudFetchSettings
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchGetCloudFetchSettings
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchGetCloudFetchSettings(body, callback) {
    const meth = 'adapter-cloudFetchGetCloudFetchSettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetCloudFetchSettingsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.GetCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchGetCloudFetchSettings failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.GetCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchGetCloudFetchSettings failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchUpdateCloudFetchSettings
   * @pronghornType method
   * @name cloudFetchUpdateCloudFetchSettings
   * @summary cloudFetchUpdateCloudFetchSettings
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchUpdateCloudFetchSettings
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchUpdateCloudFetchSettings(body, callback) {
    const meth = 'adapter-cloudFetchUpdateCloudFetchSettings';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateCloudFetchSettingsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.UpdateCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchUpdateCloudFetchSettings failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.UpdateCloudFetchSettings(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchUpdateCloudFetchSettings failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchGetAwsAccountsStatuses
   * @pronghornType method
   * @name cloudFetchGetAwsAccountsStatuses
   * @summary cloudFetchGetAwsAccountsStatuses
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchGetAwsAccountsStatuses
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchGetAwsAccountsStatuses(body, callback) {
    const meth = 'adapter-cloudFetchGetAwsAccountsStatuses';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetAwsAccountsStatusesRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.GetAwsAccountsStatuses(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchGetAwsAccountsStatuses failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.GetAwsAccountsStatuses(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchGetAwsAccountsStatuses failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchGetCloudFetchStatus
   * @pronghornType method
   * @name cloudFetchGetCloudFetchStatus
   * @summary cloudFetchGetCloudFetchStatus
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchGetCloudFetchStatus
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchGetCloudFetchStatus(body, callback) {
    const meth = 'adapter-cloudFetchGetCloudFetchStatus';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.GetCloudFetchStatusRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.GetCloudFetchStatus(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchGetCloudFetchStatus failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.GetCloudFetchStatus(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchGetCloudFetchStatus failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchAddAwsAccount
   * @pronghornType method
   * @name cloudFetchAddAwsAccount
   * @summary cloudFetchAddAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchAddAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchAddAwsAccount(body, callback) {
    const meth = 'adapter-cloudFetchAddAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.AddAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.AddAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchAddAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.AddAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchAddAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchUpdateAwsAccount
   * @pronghornType method
   * @name cloudFetchUpdateAwsAccount
   * @summary cloudFetchUpdateAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchUpdateAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchUpdateAwsAccount(body, callback) {
    const meth = 'adapter-cloudFetchUpdateAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.UpdateAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchUpdateAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.UpdateAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchUpdateAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchDeleteAwsAccount
   * @pronghornType method
   * @name cloudFetchDeleteAwsAccount
   * @summary cloudFetchDeleteAwsAccount
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchDeleteAwsAccount
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchDeleteAwsAccount(body, callback) {
    const meth = 'adapter-cloudFetchDeleteAwsAccount';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.DeleteAwsAccountRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.DeleteAwsAccount(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchDeleteAwsAccount failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.DeleteAwsAccount(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchDeleteAwsAccount failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function cloudFetchUpdateCloudFetchPollInterval
   * @pronghornType method
   * @name cloudFetchUpdateCloudFetchPollInterval
   * @summary cloudFetchUpdateCloudFetchPollInterval
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /cloudFetchUpdateCloudFetchPollInterval
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  cloudFetchUpdateCloudFetchPollInterval(body, callback) {
    const meth = 'adapter-cloudFetchUpdateCloudFetchPollInterval';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.api_gateway.api.UpdateCloudFetchPollIntervalRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.CloudFetch.UpdateCloudFetchPollInterval(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'cloudFetchUpdateCloudFetchPollInterval failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.CloudFetch.UpdateCloudFetchPollInterval(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'cloudFetchUpdateCloudFetchPollInterval failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function authGetAuthorizationMetadata
   * @pronghornType method
   * @name authGetAuthorizationMetadata
   * @summary authGetAuthorizationMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /authGetAuthorizationMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  authGetAuthorizationMetadata(body, callback) {
    const meth = 'adapter-authGetAuthorizationMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.auth.GetAuthorizationMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.Auth.GetAuthorizationMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'authGetAuthorizationMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.Auth.GetAuthorizationMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'authGetAuthorizationMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function authGetWebAccessToken
   * @pronghornType method
   * @name authGetWebAccessToken
   * @summary authGetWebAccessToken
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /authGetWebAccessToken
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  authGetWebAccessToken(body, callback) {
    const meth = 'adapter-authGetWebAccessToken';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.auth.GetWebAccessTokenRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.Auth.GetWebAccessToken(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'authGetWebAccessToken failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.Auth.GetWebAccessToken(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'authGetWebAccessToken failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationCreateChangeReview
   * @pronghornType method
   * @name changeValidationCreateChangeReview
   * @summary changeValidationCreateChangeReview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationCreateChangeReview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationCreateChangeReview(body, callback) {
    const meth = 'adapter-changeValidationCreateChangeReview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateChangeReviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.CreateChangeReview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationCreateChangeReview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.CreateChangeReview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationCreateChangeReview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationRunAllSteps
   * @pronghornType method
   * @name changeValidationRunAllSteps
   * @summary changeValidationRunAllSteps
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationRunAllSteps
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationRunAllSteps(body, callback) {
    const meth = 'adapter-changeValidationRunAllSteps';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.RunAllStepsRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.RunAllSteps(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationRunAllSteps failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.RunAllSteps(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationRunAllSteps failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetChangeReviewMetadata
   * @pronghornType method
   * @name changeValidationGetChangeReviewMetadata
   * @summary changeValidationGetChangeReviewMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetChangeReviewMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetChangeReviewMetadata(body, callback) {
    const meth = 'adapter-changeValidationGetChangeReviewMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetChangeReviewMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetChangeReviewMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationUpdateChangeReview
   * @pronghornType method
   * @name changeValidationUpdateChangeReview
   * @summary changeValidationUpdateChangeReview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationUpdateChangeReview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationUpdateChangeReview(body, callback) {
    const meth = 'adapter-changeValidationUpdateChangeReview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateChangeReviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.UpdateChangeReview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationUpdateChangeReview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.UpdateChangeReview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationUpdateChangeReview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationListChangeReviewsMetadata
   * @pronghornType method
   * @name changeValidationListChangeReviewsMetadata
   * @summary changeValidationListChangeReviewsMetadata
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationListChangeReviewsMetadata
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationListChangeReviewsMetadata(body, callback) {
    const meth = 'adapter-changeValidationListChangeReviewsMetadata';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ListChangeReviewsMetadataRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.ListChangeReviewsMetadata(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationListChangeReviewsMetadata failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.ListChangeReviewsMetadata(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationListChangeReviewsMetadata failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetChangeReviewStep
   * @pronghornType method
   * @name changeValidationGetChangeReviewStep
   * @summary changeValidationGetChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationGetChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetChangeReviewChange
   * @pronghornType method
   * @name changeValidationGetChangeReviewChange
   * @summary changeValidationGetChangeReviewChange
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetChangeReviewChange
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetChangeReviewChange(body, callback) {
    const meth = 'adapter-changeValidationGetChangeReviewChange';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewChangeRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetChangeReviewChange(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewChange failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetChangeReviewChange(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewChange failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationCreateChangeReviewStep
   * @pronghornType method
   * @name changeValidationCreateChangeReviewStep
   * @summary changeValidationCreateChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationCreateChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationCreateChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationCreateChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.CreateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationCreateChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.CreateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationCreateChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationUpdateChangeReviewStep
   * @pronghornType method
   * @name changeValidationUpdateChangeReviewStep
   * @summary changeValidationUpdateChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationUpdateChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationUpdateChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationUpdateChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.UpdateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationUpdateChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.UpdateChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationUpdateChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationRunChangeReviewStep
   * @pronghornType method
   * @name changeValidationRunChangeReviewStep
   * @summary changeValidationRunChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationRunChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationRunChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationRunChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.RunChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.RunChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationRunChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.RunChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationRunChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationDeleteChangeReviewStep
   * @pronghornType method
   * @name changeValidationDeleteChangeReviewStep
   * @summary changeValidationDeleteChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationDeleteChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationDeleteChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationDeleteChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.DeleteChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.DeleteChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationDeleteChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.DeleteChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationDeleteChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetChangeReviewStepJson
   * @pronghornType method
   * @name changeValidationGetChangeReviewStepJson
   * @summary changeValidationGetChangeReviewStepJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetChangeReviewStepJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetChangeReviewStepJson(body, callback) {
    const meth = 'adapter-changeValidationGetChangeReviewStepJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetChangeReviewStepJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStepJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetChangeReviewStepJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStepJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationExportChangeReviewStep
   * @pronghornType method
   * @name changeValidationExportChangeReviewStep
   * @summary changeValidationExportChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationExportChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationExportChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationExportChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ExportChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.ExportChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationExportChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.ExportChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationExportChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetChangeReviewStepFromJson
   * @pronghornType method
   * @name changeValidationGetChangeReviewStepFromJson
   * @summary changeValidationGetChangeReviewStepFromJson
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetChangeReviewStepFromJson
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetChangeReviewStepFromJson(body, callback) {
    const meth = 'adapter-changeValidationGetChangeReviewStepFromJson';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetChangeReviewStepFromJsonRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetChangeReviewStepFromJson(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStepFromJson failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetChangeReviewStepFromJson(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetChangeReviewStepFromJson failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationParseChangeReviewStep
   * @pronghornType method
   * @name changeValidationParseChangeReviewStep
   * @summary changeValidationParseChangeReviewStep
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationParseChangeReviewStep
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationParseChangeReviewStep(body, callback) {
    const meth = 'adapter-changeValidationParseChangeReviewStep';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.ParseChangeReviewStepRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.ParseChangeReviewStep(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationParseChangeReviewStep failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.ParseChangeReviewStep(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationParseChangeReviewStep failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationCreateStepPreview
   * @pronghornType method
   * @name changeValidationCreateStepPreview
   * @summary changeValidationCreateStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationCreateStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationCreateStepPreview(body, callback) {
    const meth = 'adapter-changeValidationCreateStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CreateStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.CreateStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationCreateStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.CreateStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationCreateStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationUpdateStepPreview
   * @pronghornType method
   * @name changeValidationUpdateStepPreview
   * @summary changeValidationUpdateStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationUpdateStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationUpdateStepPreview(body, callback) {
    const meth = 'adapter-changeValidationUpdateStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.UpdateStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.UpdateStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationUpdateStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.UpdateStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationUpdateStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationDeleteStepPreview
   * @pronghornType method
   * @name changeValidationDeleteStepPreview
   * @summary changeValidationDeleteStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationDeleteStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationDeleteStepPreview(body, callback) {
    const meth = 'adapter-changeValidationDeleteStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.DeleteStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.DeleteStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationDeleteStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.DeleteStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationDeleteStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationGetStepPreviewResult
   * @pronghornType method
   * @name changeValidationGetStepPreviewResult
   * @summary changeValidationGetStepPreviewResult
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationGetStepPreviewResult
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationGetStepPreviewResult(body, callback) {
    const meth = 'adapter-changeValidationGetStepPreviewResult';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.GetStepPreviewResultRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.GetStepPreviewResult(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationGetStepPreviewResult failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.GetStepPreviewResult(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationGetStepPreviewResult failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function changeValidationCommitStepPreview
   * @pronghornType method
   * @name changeValidationCommitStepPreview
   * @summary changeValidationCommitStepPreview
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /changeValidationCommitStepPreview
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  changeValidationCommitStepPreview(body, callback) {
    const meth = 'adapter-changeValidationCommitStepPreview';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.change_validation.CommitStepPreviewRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.ChangeValidation.CommitStepPreview(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'changeValidationCommitStepPreview failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.ChangeValidation.CommitStepPreview(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'changeValidationCommitStepPreview failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function healthHealthCheck
   * @pronghornType method
   * @name healthHealthCheck
   * @summary healthCheck
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /healthHealthCheck
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  healthHealthCheck(body, callback) {
    const meth = 'adapter-healthHealthCheck';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.health.HealthCheckRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          return this.clients.Health.HealthCheck(from$body, workbench.metadata, (error, response) => {
            if (error) {
              const errorObj = formatErrorObject(origin, 'healthHealthCheck failed', null, null, null, error);
              log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
              return callback(null, errorObj);
            }

            return callback({
              status: 'success',
              code: 200,
              response: workbench.walkResponse(response)
            });
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      return this.clients.Health.HealthCheck(from$body, workbench.metadata, (error, response) => {
        if (error) {
          const errorObj = formatErrorObject(origin, 'healthHealthCheck failed', null, null, null, error);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }

        return callback({
          status: 'success',
          code: 200,
          response: workbench.walkResponse(response)
        });
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }

  /**
   * @function healthWatch
   * @pronghornType method
   * @name healthWatch
   * @summary healthWatch
   *
   * @param {object} body - body param
   * @param {getCallback} callback - a callback function to return the result
   * @return {object} results - An object containing the response of the action
   *
   * @route {POST} /healthWatch
   * @roles admin
   * @task true
   */
  /* YOU CAN CHANGE THE PARAMETERS YOU TAKE IN HERE AND IN THE pronghorn.json FILE */
  healthWatch(body, callback) {
    const meth = 'adapter-healthWatch';
    const origin = `${this.id}-${meth}`;
    log.trace(origin);

    try {
      /* HERE IS WHERE YOU VALIDATE DATA */
      if (body === undefined || body === null || body === '') {
        const errorObj = formatErrorObject(origin, 'Missing Data', ['body'], null, null, null);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        return callback(null, errorObj);
      }

      const fullName = '.com.intentionet.bfe.proto.health.HealthCheckRequest';
      let from$body;
      if (workbench.getJpath(body, 'fullName') !== fullName) {
        const typeRefl = workbench.root.lookupType(fullName);
        from$body = typeRefl.fromObject(body);
        const validationErr = typeRefl.verify(from$body);
        if (validationErr) {
          const errorObj = formatErrorObject(origin, 'Validation failed', null, null, null, validationErr);
          log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
          return callback(null, errorObj);
        }
      }

      if (this.props.authentication && this.props.authentication.auth_method === 'request_token') {
        return this.getToken((tokenObj, terror) => {
          if (terror) {
            return callback(null, terror);
          }
          workbench.updateCallToken(tokenObj);

          // if we are running in stub mode just return without making the call
          if (this.props.stub === true) {
            return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
          }
          const WatchResponse = [];
          const WatchReadable = this.clients.Health.Watch(from$body, workbench.metadata);
          WatchReadable.on('data', (data) => {
            WatchResponse.push(workbench.walkResponse(data));
          });
          WatchReadable.on('error', (error) => {
            const errorObj = formatErrorObject(origin, 'healthWatch failed', null, null, null, error);
            log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
            const len = WatchResponse.length;
            log.error(`${origin}- WatchResponse length is ${len}`);
            if (len && util) {
              log.debug(`${origin}= last item received: ${util.inspect(WatchResponse[len - 1])}`);
            }
            return callback(null, errorObj);
          });
          WatchReadable.on('end', () => callback({
            status: 'success',
            code: 200,
            response: WatchResponse
          }));
          WatchReadable.on('status', (status) => {
            log.debug(`${origin} - WatchReadable status: ${util.inspect(status, { depth: 3, colors: true })}`);
          });
        });
      }

      // if we are running in stub mode just return without making the call
      if (this.props.stub === true) {
        return callback(null, formatErrorObject(origin, 'Error On Request', [400], null, null, null));
      }
      const WatchResponse = [];
      const WatchReadable = this.clients.Health.Watch(from$body, workbench.metadata);
      WatchReadable.on('data', (data) => {
        WatchResponse.push(workbench.walkResponse(data));
      });
      WatchReadable.on('error', (error) => {
        const errorObj = formatErrorObject(origin, 'healthWatch failed', null, null, null, error);
        log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
        const len = WatchResponse.length;
        log.error(`${origin}- WatchResponse length is ${len}`);
        if (len && util) {
          log.debug(`${origin}= last item received: ${util.inspect(WatchResponse[len - 1])}`);
        }
        return callback(null, errorObj);
      });
      WatchReadable.on('end', () => callback({
        status: 'success',
        code: 200,
        response: WatchResponse
      }));
      WatchReadable.on('status', (status) => {
        log.debug(`${origin} - WatchReadable status: ${util.inspect(status, { depth: 3, colors: true })}`);
      });
    } catch (ex) {
      const errorObj = formatErrorObject(origin, 'Caught Exception', null, null, null, ex);
      log.error(`${origin}: ${errorObj.IAPerror.displayString}`);
      return callback(null, errorObj);
    }
  }
}

module.exports = Batfish;
