# _Deprecation Notice_
This Adapter has been deprecated as of 07-15-2024 and will be end of life on 07-15-2025. The capabilities of this Adapter have not been replaced. If you are using this adapter and would like to see Itential maintain it please contact Product Support or your Customer Success Team.

# Adapter for Batfish

This adapter is used to integrate the Itential Automation Platform (IAP) with the Batfish System. The API for Batfish is available at [undefined API URL]. The adapter utilizes the Batfish API to provide the integrations that are deemed pertinent to IAP. This ReadMe file is intended to provide information on this adapter.

>**Note**: It is possible that some integrations will be supported through the Batfish adapter while other integrations will not.

Itential provides information on all of its product adapters in the Customer Knowledge Base. Information in the [Customer Knowledge Base](https://itential.atlassian.net/servicedesk/customer/portals) is consistently maintained and goes through documentation reviews. As a result, it should be the first place to go for information.

For custom built adapters, it is a starting point to understand what you have built, provide the information for you to be able to update the adapter, and assist you with deploying the adapter into IAP.

## Versioning

Itential Product adapters utilize SemVer for versioning. The current version of the adapter can be found in the `package.json` file or viewed in the IAP GUI on the System page. For Open Source Adapters, the versions available can be found in the [Itential OpenSource Repository](https://www.npmjs.com/search?q=itentialopensource%2Fadapter).

## Release History

Any release prior to 1.0.0 is a pre-release. Initial builds of adapters are generally set up as pre-releases as there is often work that needs to be done to configure the adapter and make sure the authentication process to Batfish works appropriately.

Release notes can be viewed in CHANGELOG.md or in the [Customer Knowledge Base](https://itential.atlassian.net/servicedesk/customer/portals) for Itential adapters.

## Getting Started

These instructions will help you get a copy of the project on your local machine for development and testing. Reading this section is also helpful for deployments as it provides you with pertinent information on prerequisites and properties.

### Adapter Technical Resources

There is adapter documentation available on the Itential Developer Site [HERE](https://developer.itential.io/adapters-resources/). This documentation includes information and examples that are helpful for:

```text
Authentication
Properties
Code Files
Action Files
Schema Files
Mock Data Files
Linting and Testing
Troubleshooting
```

Others will be added over time.
Want to build a new adapter? Use the Adapter Builder [HERE](https://adapters.itential.io)

### Environment Prerequisites

The following is a list of required packages for an adapter.

```text
Node.js
npm
Git
```

### Adapter Prerequisites

The following list of packages are required for Itential product adapters or custom adapters that have been built utilizing the Itential Adapter Builder.

| Package | Description |
| ------- | ------- |
| @itentialopensource/adapter-utils | Runtime library classes for all adapters;  includes request handling, connection, throttling, and translation. |
| ajv | Required for validation of adapter properties to integrate with Batfish. |
| commander | Utilized by the node scripts that are included with the adapter; helps to build and extend the functionality. |
| fs-extra | Utilized by the node scripts that are included with the adapter; helps to build and extend the functionality. |
| network-diagnostics | Utilized by the node scripts that are included with the adapter; helps to build and extend the functionality. |
| readline-sync | Utilized by the node script that comes with the adapter;  helps to test unit and integration functionality. |
| semver | Utilized by the node scripts that are included with the adapter; helps to build and extend the functionality. |

Some of the adapter node scripts run testing scripts which require the dev dependencies listed below.

### Additional Prerequisites for Development and Testing

If you are developing and testing a custom adapter, or have testing capabilities on an Itential product adapter, you will need to install these packages as well.

```text
chai
eslint
eslint-config-airbnb-base
eslint-plugin-import
eslint-plugin-json
mocha
mocha-param
nyc
package-json-validator
testdouble
winston
```

### Creating a Workspace

The following provides a local copy of the repository along with adapter dependencies.

```bash
git clone git@gitlab.com:\@itentialopensource/adapters/adapter-batfish
npm install
```

## Adapter Properties and Descriptions

This section defines **all** the properties that are available for the adapter, including detailed information on what each property is for. If you are not using certain capabilities with this adapter, you do not need to define all of the properties. An example of how the properties for this adapter can be used with tests or IAP are provided in the **Installation** section.

```json
{
  "id": "Adapter-batfish",
  "type": "Batfish",
  "properties": {
    "host": "iap-demo.intentionet.com",
    "port": 443,
    "stub": false,
    "authentication": {
      "auth_method": "static_token",
      "username": "username",
      "password": "password",
      "token": "token",
      "token_timeout": 600000,
      "token_cache": "local",
      "invalid_token_error": 401,
      "auth_field": "header.headers.Authorization",
      "auth_field_format": "Bearer {token}",
      "auth_logging": false,
      "client_id": "",
      "client_secret": "",
      "grant_type": ""
    },
    "ssl": {
      "ecdhCurve": "",
      "enabled": false,
      "accept_invalid_cert": false,
      "ca_file": "",
      "key_file": "",
      "cert_file": "",
      "secure_protocol": "",
      "ciphers": ""
    }
  },
  "groups": [],
  "brokers": [],
  "logLevel": "none",
  "timeout": 120000
}
```

### Connection Properties

These base properties are used to connect to Batfish upon the adapter initially coming up. It is important to set these properties appropriately.

| Property | Description |
| ------- | ------- |
| host | Required. A fully qualified domain name or IP address.|
| port | Required. Used to connect to the server.|
| stub | Optional. Indicates whether the stub should run instead of making calls to Batfish (very useful during basic testing). Default is false (which means connect to Batfish).|

A connectivity check tells IAP the adapter has loaded successfully.


## Testing an Itential Product Adapter

Mocha is generally used to test all Itential Product Adapters. There are unit tests as well as integration tests performed. Integration tests can generally be run as standalone using mock data and running the adapter in stub mode, or as integrated. When running integrated, every effort is made to prevent environmental failures, however there is still a possibility.

### Unit Testing

Unit Testing includes testing basic adapter functionality as well as error conditions that are triggered in the adapter prior to any integration. There are two ways to run unit tests. The prefered method is to use the testRunner script; however, both methods are provided here.


```bash
node utils/testRunner --unit

npm run test:unit
```

To add new unit tests, edit the `test/unit/adapterTestUnit.js` file. The tests that are already in this file should provide guidance for adding additional tests.

### Integration Testing - Standalone

Standalone Integration Testing requires mock data to be provided with the entities. If this data is not provided, standalone integration testing will fail. When the adapter is set to run in stub mode (setting the stub property to true), the adapter will run through its code up to the point of making the request. It will then retrieve the mock data and return that as if it had received that data as the response from Batfish. It will then translate the data so that the adapter can return the expected response to the rest of the Itential software. Standalone is the default integration test.

Similar to unit testing, there are two ways to run integration tests. Using the testRunner script is better because it prevents you from having to edit the test script; it will also resets information after testing is complete so that credentials are not saved in the file.

```bash
node utils/testRunner
  answer no at the first prompt

npm run test:integration
```

To add new integration tests, edit the `test/integration/adapterTestIntegration.js` file. The tests that are already in this file should provide guidance for adding additional tests.

### Integration Testing

Integration Testing requires connectivity to Batfish. By using the testRunner script it prevents you from having to edit the integration test. It also resets the integration test after the test is complete so that credentials are not saved in the file.

> **Note**: These tests have been written as a best effort to make them work in most environments. However, the Adapter Builder often does not have the necessary information that is required to set up valid integration tests. For example, the order of the requests can be very important and data is often required for `creates` and `updates`. Hence, integration tests may have to be enhanced before they will work (integrate) with Batfish. Even after tests have been set up properly, it is possible there are environmental constraints that could result in test failures. Some examples of possible environmental issues are customizations that have been made within Batfish which change order dependencies or required data.

```bash
node utils/testRunner
answer yes at the first prompt
answer all other questions on connectivity and credentials
```

Test should also be written to clean up after themselves. However, it is important to understand that in some cases this may not be possible. In addition, whenever exceptions occur, test execution may be stopped, which will prevent cleanup actions from running. It is recommended that tests be utilized in dev and test labs only.

> **Reminder**: Do not check in code with actual credentials to systems.

## Adapter Node Scripts

There are several node scripts that now accompany the adapter. These scripts are provided to make several activities easier. Each of these scripts are described below.

| Run | Description |
| ------- | ------- |
| npm run adapter:install | Provides an easier way to install the adapter.|
| npm run adapter:checkMigrate | Checks whether your adapter can and should be migrated to the latest foundation.|
| npm run adapter:findPath | Can be used to see if the adapter supports a particular API call.|
| npm run adapter:migrate | Provides an easier way to migrate your adapter after you download the migration zip from Itential DevSite|
| npm run adapter:update | Provides an easier way to update your adapter after you download the migration zip from Itential DevSite|
| npm run adapter:revert | Allows you to revert after a migration or update if it resulted in issues.|
| npm run troubleshoot | Provides a way to troubleshoot the adapter - runs connectivity, healthcheck and basic get.|
| npm run connectivity | Provides a connectivity check to the Batfish system.|
| npm run healthcheck | Checks whether the configured healthcheck call works to Batfish.|
| npm run basicget | Checks whether the basic get calls works to Batfish.|

## Installing an Itential Product Adapter

If you have App-Artifact installed in IAP, you can follow the instruction for that application to install the adapter into IAP. If not, follow these instructions.

1. Set up the name space location in your IAP node_modules.

```bash
cd /opt/pronghorn/current/node_modules
if the @itentialopensource directory does not exist, create it:
   mkdir @itentialopensource
```

2. Clone the adapter into your IAP environment.

```bash
cd \@itentialopensource
git clone git@gitlab.com:\@itentialopensource/adapters/adapter-batfish
```

3. Run the adapter install script.

```bash
cd adapter-batfish
npm run adapter:install
```

4. Restart IAP

```bash
systemctl restart pronghorn
```

## Installing a Custom Adapter

If you built this as a custom adapter through the Adapter Builder, it is recommended you go through setting up a development environment and testing the adapter before installing it. There is often configuration and authentication work that is required before the adapter will work in IAP.

1. Move the adapter into the IAP `node_modules` directory.

```text
Depending on where your code is located, this process is different.
    Could be a tar, move, untar
    Could be a git clone of a repository
    Could also be a cp -R from a coding directory
Adapter should be placed into: /opt/pronghorn/current/node_modules/\@itentialopensource
```

2. Follow Steps 3-4 (above) to install an Itential adapter to load your properties, dependencies and restart IAP.

## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Specific Adapter Calls

Specific adapter calls are built based on the API of the Batfish. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.


### Functional Issues

Adapter logs are located in `/var/log/pronghorn`. In older releases of the Itential Platform, there is a `pronghorn.log` file which contains logs for all of the Itential Platform. In newer versions, adapters are logging into their own files.

## Contributing to Batfish

Please check out the [Contributing Guidelines](./CONTRIBUTING.md).

## License & Maintainers

### Maintained By

```text
Itential Product Adapters are maintained by the Itential Adapter Team.
Itential OpenSource Adapters are maintained by the community at large.
Custom Adapters are maintained by other sources.
```

### Product License

[Apache 2.0](./LICENSE)
