const JSON_DESCRIPTOR_FILENAME = 'jsonDescriptor.json';

// UTILITIES
const fs = require('fs');
const util = require('util');
const path = require('path');
const Long = require('long');

// GRPC & PROTOBUF PACKAGES
const protobufjs = require('protobufjs');
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader'); // IMPORTANT must be at least 0.6.0

// ONE-LINERS
const addpath = (propPath, add) => (propPath ? (add ? `${propPath}.${add}` : propPath) : add); // eslint-disable-line no-nested-ternary
const getFullName = (obj, fname) => fname.split('.').reduce((ob, prop) => ob && (prop ? ob[prop] : ob), obj);
const getJpath = (obj, propPath) => propPath.split('.').reduce((ob, prop) => ob && ob[prop], obj);
function look(obj, depth = 0) { console.log(util.inspect(obj, { colors: true, depth })); }
// function mem() { console.log(`Memory: ${util.inspect(process.memoryUsage())}`); }
// const onlyOne = (...args) => args.reduce((acc, v) => acc + (v !== null), 0) === 1;

// WORKING DATA STRUCTURES
const inventory = {
  services: {},
  shortnames: {},
  shortuniq: {},
  reverse_shortuniq: {},
  methods: {},
  requests: {},
  responses: {}
};
const work = {
  bundleDefinition: {},
  bundleDescriptor: {},
  clients: {},
  jsonDescriptor: '{}',
  jsonDescriptorFilename: '',
  loadOptions: {},
  parsedDescriptor: {},
  root: {},
  metadata: new grpc.Metadata()
};
work.loadOptions = {
  keepCase: true,
  longs: 'string',
  enums: 'string',
  defaults: true,
  oneofs: true
};
work.jsonDescriptorFilename = path.resolve(__dirname, JSON_DESCRIPTOR_FILENAME);

// SET UP DEFINITIONS AND REFLECTION CLASSES FROM THE JSON DESCRIPTOR FILE
function getJsonDescriptor() {
  work.jsonDescriptor = fs.readFileSync(work.jsonDescriptorFilename);
  work.parsedDescriptor = JSON.parse(work.jsonDescriptor);
  if (!getJpath(work, 'parsedDescriptor.options')) work.parsedDescriptor.options = work.loadOptions;
}

function loadJsonDescriptor() {
  work.bundleDefinition = protoLoader.fromJSON(work.parsedDescriptor, work.loadOptions);
}

function getRoot() {
  work.root = protobufjs.Root.fromJSON(work.parsedDescriptor);
  work.root.resolveAll();
}

// ###################
// DESCRIPTOR ANALYSIS
// ###################
const serviceList = {};
const messageList = {};
const enumList = {};
let nestMsgTally = 0;
let nestEnumTally = 0;

function getTypeAndSubtypes(typeobj, propPath) {
  const nested = typeobj.nestedType;
  const enums = typeobj.enumType;
  messageList[propPath] = {};
  if (nested && nested.length) {
    nested.forEach((item) => { getTypeAndSubtypes(item, addpath(propPath, item.name)); nestMsgTally += 1; });
  }
  if (enums && enums.length) {
    enums.forEach((item) => { enumList[addpath(propPath, item.name)] = {}; nestEnumTally += 1; });
  }
}

function handleProtoDescriptors(typeobj, format, propPath) {
  switch (format) {
    case 'DescriptorProto':
      getTypeAndSubtypes(typeobj, propPath);
      break;
    case 'EnumDescriptorProto':
      enumList[propPath] = {};
      break;
    default:
      console.error(`handleProtoDescriptors: unhandled case: ${format}`);
  }
}

function handleService(service, propPath) {
  serviceList[propPath] = {};
  const now = serviceList[propPath];
  Object.getOwnPropertyNames(service).forEach((rpc) => {
    now[rpc] = {
      request: getJpath(service[rpc], 'requestType.type.name') || null,
      response: getJpath(service[rpc], 'responseType.type.name') || null
    };
  });
}

function walkDescriptorTree(node, propPath = '') {
  // console.error( `walkDescriptorTree: propPath is ${propPath}` );
  if (typeof node !== 'object') return;
  Object.getOwnPropertyNames(node).forEach((prop) => {
    // console.error( `walkDescriptorTree: prop is ${prop}`);
    if (node[prop] === null) return;
    switch (typeof node[prop]) {
      case 'function':
        const svc = node[prop].service; // eslint-disable-line no-case-declarations
        if (svc) {
          handleService(svc, addpath(propPath, prop));
        } else {
          console.error(`function ${addpath(propPath, prop)} doesn't look like a service`);
        }
        break;
      case 'object':
        // if( Array.isArray( node[prop] )){ node[prop].forEach( (element, index) => { walkDescriptorTree( element, addpath( propPath, `${prop}[${index}]` ) ); }); }
        const fmt = node[prop].format; // eslint-disable-line no-case-declarations
        if (fmt && fmt.startsWith('Protocol Buffer 3')) {
          handleProtoDescriptors(node[prop].type, fmt.substring(18), addpath(propPath, prop));
        } else {
          walkDescriptorTree(node[prop], addpath(propPath, prop));
        }
        break;
      default:
    }
  });
}

function getDefinitionLists(bundleDescriptor) { // eslint-disable-line no-unused-vars
  // needs get_json_bundle, load_json_bundle, bundleDescriptor
  try {
    walkDescriptorTree(bundleDescriptor);
  } catch (e) {
    console.error(util.inspect(e));
  }
  console.error(`service count: ${Object.getOwnPropertyNames(serviceList).length}\nmessage count: ${Object.getOwnPropertyNames(messageList).length}, (${nestMsgTally} nested)\nenum    count: ${Object.getOwnPropertyNames(enumList).length}, (${nestEnumTally} nested)\n`);
  console.log(JSON.stringify({ serviceList, messageList, enumList }, null, 2));
}

// ###################
// REFLECTION ANALYSIS
// ###################
function walkReflectionTree(node, propPath = '') {
  // eslint-disable-next-line one-var-declaration-per-line
  let classname, meth, req, rreq, rsp, rrsp, nest; // eslint-disable-line one-var
  if (typeof node !== 'object') return;
  Object.getOwnPropertyNames(node).forEach((prop) => {
    // console.error( `walkReflectionTree: prop is ${prop}`);
    if (node[prop] === null) return;
    if (typeof node[prop] === 'object') {
      if (classname = getJpath(node[prop], 'constructor.name')) { // eslint-disable-line no-cond-assign
        switch (classname) {
          case 'Service':
            // add full name and reflection to inventory
            inventory.services[node[prop].fullName] = node[prop];
            // add short name (might not be unique so use arrays)
            if (!inventory.shortnames[prop]) inventory.shortnames[prop] = [];
            inventory.shortnames[prop].push(node[prop].fullName);
            // collect the methods (rpc calls)
            if (meth = node[prop].methods) { // eslint-disable-line no-cond-assign
              walkReflectionTree(meth, addpath(propPath, prop));
            }
            break;
          case 'Method':
            // make sure the method is resolved (subcomponents are instantiated)
            if (!node[prop].resolved && node[prop].resolve) node[prop].resolve();
            // add to inventory
            inventory.methods[`${propPath}.${prop}`] = node[prop];
            // add request name and reflection to inventory
            if ((req = node[prop].requestType) && (rreq = node[prop].resolvedRequestType)) { // eslint-disable-line no-cond-assign
              inventory.requests[`${propPath}.${prop}.${req}`] = rreq;
            }
            // add response name and reflection to inventory
            if ((rsp = node[prop].responseType) && (rrsp = node[prop].resolvedResponseType)) { // eslint-disable-line no-cond-assign
              inventory.responses[`${propPath}.${prop}.${rsp}`] = rrsp;
            }
            break;
          case 'Namespace':
          case 'Root':
            if (nest = node[prop].nested) { // eslint-disable-line no-cond-assign
              walkReflectionTree(nest, addpath(propPath, prop));
            }
            break;
          default:
        }
      }
    }
  });
}

function disambiguateNames(ambiguous, names) {
  /* eslint-disable no-loop-func */
  /* eslint-disable no-param-reassign */
  const atoms = names.map((name) => name.split('.'));
  // 0. deprecated: save the last part (now we're just using ambiguous)
  // let last = atoms[0][atoms[0].length-1] ;
  let hold = [];
  // 1. strip off leading atom(s) from each name and discard if identical
  do {
    hold = [];
    atoms.forEach((atom) => hold.push(atom.shift()));
  } while (hold.every((bit) => bit === hold[0]));
  atoms.forEach((atom) => atom.unshift(hold.shift()));
  // 2. strip off trailing atom(s) from each name and discard if identical
  // 3. append the ambiguous short name (ie ambiguous)
  do {
    hold = [];
    atoms.forEach((atom) => hold.push(atom.pop()));
  } while (hold.every((bit) => bit === hold[0]));
  atoms.forEach((atom) => atom.push(hold.shift(), ambiguous));
  // deprecated: atoms.forEach( atom => atom.push( last ));
  // atoms.forEach( atom => atom.push( ambiguous ));
  // 4. create new names of CamelCaseAtoms and reverse lookup
  const result = atoms.reduce((pairs, atom, ind) => {
    const uniq = atom.reduce((acc, cur) => acc + cur.substring(0, 1).toUpperCase() + cur.substring(1), '');
    pairs[uniq] = names[ind];
    pairs[names[ind]] = uniq;
    return pairs;
  }, {});
  /*
  console.error( 'names: '+ util.inspect( names ));
  console.error( 'atoms: '+ util.inspect( atoms ));
  console.error( 'result: '+ util.inspect( result ));
  */
  return result;
}

function disambiguateServices(services) {
  /* eslint-disable prefer-destructuring */
  return Object.getOwnPropertyNames(services).reduce((uniq, svc) => {
    const ssvc = services[svc];
    if (ssvc.length === 1) {
      uniq[svc] = ssvc[0];
      uniq[ssvc[0]] = svc;
    } else {
      Object.assign(uniq, disambiguateNames(svc, ssvc));
    }
    return uniq;
  }, {});
}

function decorateServiceReflections() {
  // call AFTER disambiguateServices
  Object.getOwnPropertyNames(inventory.services).forEach((service) => {
    inventory.services[service].shortuniq = inventory.shortuniq[service];
  });
}

// ############
// GRPC CLIENTS
// ############
function instantiateClients(url, rootCertPath = '') {
  let rootCert;
  let sslCreds;
  work.clients = {};
  try {
    if (rootCertPath) {
      rootCert = fs.readFileSync(rootCertPath);
      sslCreds = grpc.credentials.createSsl(rootCert, null, null, { rejectUnauthorized: false });
      // console.error(`rootCert is ${rootCert}`);
    }
    Object.getOwnPropertyNames(inventory.shortuniq).filter((name) => !name.startsWith('.')).forEach((svcname) => {
      work.clients[svcname] = new (getFullName(work.bundleDescriptor, inventory.shortuniq[svcname]))(url, (rootCertPath ? sslCreds : grpc.credentials.createInsecure()));
    });
    return work.clients;
  } catch (e) {
    log.error(`adapterGrpcBase::instantiateClients caught ${util.inspect(e)}`); // eslint-disable-line no-undef
  }
  return null;
}

function setCallTokenHeaderName(newName) {
  // work.metadata = new grpc.Metadata(); // this is being done during the init of the work object above
  if (newName) {
    work.callTokenHeaderName = newName;
    work.metadata.add(work.callTokenHeaderName, 'INITIAL_DUMMY_VALUE');
  }
}

function updateCallToken(newValue) {
  if (work.callTokenHeaderName) {
    work.metadata.set(work.callTokenHeaderName, newValue);
    // console.error(`metadata is ${util.inspect(work.metadata, {depth:Infinity, colors:true})}`);
  }
}

function walkResponse(respObj) {
  if (typeof respObj !== 'object' || respObj === null || respObj instanceof Function) return respObj;
  if (respObj instanceof Long) return respObj.toString();
  if (Array.isArray(respObj)) return respObj.map(walkResponse);
  return Object.getOwnPropertyNames(respObj).reduce((acc, key) => ({
    ...acc, [key]: (respObj[key] instanceof Long ? respObj[key].toString() : walkResponse(respObj[key]))
  }), {});
}

function init() {
  getJsonDescriptor();
  getRoot();
  loadJsonDescriptor();
  work.bundleDescriptor = grpc.loadPackageDefinition(work.bundleDefinition);
  walkReflectionTree(work.root.nested);
  inventory.shortuniq = disambiguateServices(inventory.shortnames);
  decorateServiceReflections();
  exposeWorkProperties(); // eslint-disable-line no-use-before-define
}

// #######
// EXPORTS
// #######
function exposeWorkProperties() {
  module.exports.root = work.root;
  module.exports.json = work.jsonBundle;
  module.exports.definitions = work.bundleDefinition;
  module.exports.descriptors = work.bundleDescriptor;
  // module.exports.swagger = THIS IS CURRENTLY INTERNAL TO protobuf2swagger
  module.exports.openapi3 = work.swagger_bundle;
}

function graft(key, value) {
  module.exports[key] = value;
}

module.exports = {
  init,
  instantiateClients,
  work,
  inventory,
  getJpath,
  getFullName,
  look,
  graft,
  updateCallToken,
  setCallTokenHeaderName,
  get metadata() { return work.metadata; },
  set metadata(val) { work.metadata = val; },
  walkResponse
};
