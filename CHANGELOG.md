
## 0.1.2 [09-27-2021]

* Bug fixes and performance improvements

See commit 45a66a5

---

## 0.1.1 [09-24-2021]

* Bug fixes and performance improvements

See commit a2941a4

---
