/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const mocha = require('mocha');
const path = require('path');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const isRapidFail = false;
const isSaveMockData = false;
const attemptTimeout = 5000;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
const host = 'replace.hostorip.here';
const username = 'username';
const password = 'password';
const protocol = 'http';
const port = 80;
const sslenable = false;
const sslinvalid = false;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-batfish',
      type: 'Batfish',
      properties: {
        host,
        port,
        stub,
        authentication: {
          auth_method: 'static_token',
          username,
          password,
          token: 'cacacacasdcdsc',
          invalid_token_error: 401,
          token_timeout: -1,
          token_cache: 'local',
          auth_field: 'header.headers.Authorization',
          auth_field_format: 'Bearer {token}',
          auth_logging: false,
          client_id: '',
          client_secret: '',
          grant_type: ''
        },
        ssl: {
          ecdhCurve: '',
          enabled: sslenable,
          accept_invalid_cert: sslinvalid,
          ca_file: '',
          key_file: '',
          cert_file: '',
          secure_protocol: '',
          ciphers: ''
        }
      }
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Batfish = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Batfish Adapter Test', () => {
  describe('Batfish Class Tests', () => {
    const a = new Batfish(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const parserParserParseBodyParam = {
      filename: 'string',
      contents: []
    };
    describe('#parserParse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.parserParse(parserParserParseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-parserParse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Parser', 'parserParse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayActivateLicenseBodyParam = {
      activationCode: []
    };
    describe('#apiGatewayActivateLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayActivateLicense(apiGatewayApiGatewayActivateLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayActivateLicense', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayActivateLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayAddAwsAccountBodyParam = {
      account: {
        name: 'string',
        roleArn: 'string',
        regions: [
          'string'
        ],
        accountId: 'string'
      }
    };
    describe('#apiGatewayAddAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayAddAwsAccount(apiGatewayApiGatewayAddAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayAddAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayAddAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCommitStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      },
      stepId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayCommitStepPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCommitStepPreview(apiGatewayApiGatewayCommitStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCommitStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCommitStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreateAdHocAssertionBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      assertion: {
        id: {
          uid: 'string'
        },
        input: {
          input: null,
          description: 'string',
          title: 'string'
        }
      },
      options: {
        hidden: true
      }
    };
    describe('#apiGatewayCreateAdHocAssertion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCreateAdHocAssertion(apiGatewayApiGatewayCreateAdHocAssertionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreateAdHocAssertion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreateAdHocAssertion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreateChangeReviewBodyParam = {
      title: 'string',
      description: 'string',
      networkName: 'string',
      base: null,
      changeSnapshotName: 'string',
      change: {
        fileChanges: [
          {
            fileName: 'string',
            changeText: 'string'
          }
        ],
        newFiles: [
          {
            fileName: 'string',
            content: 'string'
          }
        ],
        deactivateDevices: [
          'string'
        ],
        deactivateInterfaces: [
          {
            hostname: 'string',
            interface: 'string'
          }
        ]
      },
      steps: [
        {
          step: null
        }
      ]
    };
    describe('#apiGatewayCreateChangeReview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCreateChangeReview(apiGatewayApiGatewayCreateChangeReviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreateChangeReview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreateChangeReview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreateChangeReviewStepBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      },
      step: null
    };
    describe('#apiGatewayCreateChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCreateChangeReviewStep(apiGatewayApiGatewayCreateChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreateChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreateChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreateNetworkBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      nonce: 9
    };
    describe('#apiGatewayCreateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayCreateNetwork(apiGatewayApiGatewayCreateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreateNetwork', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreatePolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policy: {
        uid: {
          uid: 'string'
        },
        input: {
          input: null,
          description: 'string',
          title: 'string'
        }
      }
    };
    describe('#apiGatewayCreatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCreatePolicy(apiGatewayApiGatewayCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreatePolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayCreateStepPreviewBodyParam = {
      changeReviewId: {
        uid: 'string',
        network: 'string'
      },
      step: {
        step: null
      }
    };
    describe('#apiGatewayCreateStepPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayCreateStepPreview(apiGatewayApiGatewayCreateStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayCreateStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayCreateStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteAwsAccountBodyParam = {
      accountId: 'string'
    };
    describe('#apiGatewayDeleteAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeleteAwsAccount(apiGatewayApiGatewayDeleteAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayDeleteChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeleteChangeReviewStep(apiGatewayApiGatewayDeleteChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteNetworkBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      nonce: 1
    };
    describe('#apiGatewayDeleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeleteNetwork(apiGatewayApiGatewayDeleteNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteNetwork', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeletePolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policyId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayDeletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeletePolicy(apiGatewayApiGatewayDeletePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeletePolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteQuestionBodyParam = {
      networkName: 'string',
      questionName: 'string'
    };
    describe('#apiGatewayDeleteQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayDeleteQuestion(apiGatewayApiGatewayDeleteQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string'
    };
    describe('#apiGatewayDeleteReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayDeleteReferenceBook(apiGatewayApiGatewayDeleteReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      nonce: 10
    };
    describe('#apiGatewayDeleteSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeleteSnapshot(apiGatewayApiGatewayDeleteSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayDeleteStepPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayDeleteStepPreview(apiGatewayApiGatewayDeleteStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteTopologyAggregatesBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayDeleteTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayDeleteTopologyAggregates(apiGatewayApiGatewayDeleteTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteTopologyPositionsBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayDeleteTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayDeleteTopologyPositions(apiGatewayApiGatewayDeleteTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayDeleteTopologyRootsBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayDeleteTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayDeleteTopologyRoots(apiGatewayApiGatewayDeleteTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayDeleteTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayDeleteTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayExportAssertionInputBodyParam = {
      assertionInput: {
        input: null,
        description: 'string',
        title: 'string'
      }
    };
    describe('#apiGatewayExportAssertionInput - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayExportAssertionInput(apiGatewayApiGatewayExportAssertionInputBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayExportAssertionInput', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayExportAssertionInput', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayExportChangeReviewStepBodyParam = {
      step: {
        step: null
      }
    };
    describe('#apiGatewayExportChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayExportChangeReviewStep(apiGatewayApiGatewayExportChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayExportChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayExportChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayForkFromMasterSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      nonce: 9,
      debugFlags: [
        'string'
      ],
      disableParseReuse: false,
      files: [
        {
          key: 'string',
          contents: []
        }
      ],
      removeFiles: [
        {
          key: 'string'
        }
      ],
      removeDirs: [
        {
          key: 'string'
        }
      ]
    };
    describe('#apiGatewayForkFromMasterSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayForkFromMasterSnapshot(apiGatewayApiGatewayForkFromMasterSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayForkFromMasterSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayForkFromMasterSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayForkSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      zipData: [],
      debugFlags: [
        'string'
      ],
      disableParseReuse: false,
      nonce: 10,
      baseSnapshotName: 'string',
      deactivateNodes: [
        {
          name: 'string'
        }
      ],
      restoreNodes: [
        {
          name: 'string'
        }
      ],
      deactivateInterfaces: [
        {
          nodeName: 'string',
          interfaceName: 'string'
        }
      ],
      restoreInterfaces: [
        {
          nodeName: 'string',
          interfaceName: 'string'
        }
      ],
      files: [
        {
          key: 'string',
          contents: []
        }
      ],
      options: {
        hidden: false
      }
    };
    describe('#apiGatewayForkSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayForkSnapshot(apiGatewayApiGatewayForkSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayForkSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayForkSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetAdHocAssertionBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetAdHocAssertion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetAdHocAssertion(apiGatewayApiGatewayGetAdHocAssertionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetAdHocAssertion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetAdHocAssertion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetAdHocAssertionResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetAdHocAssertionResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetAdHocAssertionResult(apiGatewayApiGatewayGetAdHocAssertionResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetAdHocAssertionResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetAdHocAssertionResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetAssertionInputJsonBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      assertionInput: {
        input: null,
        description: 'string',
        title: 'string'
      }
    };
    describe('#apiGatewayGetAssertionInputJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetAssertionInputJson(apiGatewayApiGatewayGetAssertionInputJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetAssertionInputJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetAssertionInputJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetAwsAccountsStatusesBodyParam = {};
    describe('#apiGatewayGetAwsAccountsStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetAwsAccountsStatuses(apiGatewayApiGatewayGetAwsAccountsStatusesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetAwsAccountsStatuses', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetAwsAccountsStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetChangeReviewChangeBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#apiGatewayGetChangeReviewChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetChangeReviewChange(apiGatewayApiGatewayGetChangeReviewChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetChangeReviewChange', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetChangeReviewChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetChangeReviewMetadataBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#apiGatewayGetChangeReviewMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetChangeReviewMetadata(apiGatewayApiGatewayGetChangeReviewMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetChangeReviewMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetChangeReviewMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetChangeReviewStep(apiGatewayApiGatewayGetChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetChangeReviewStepFromJsonBodyParam = {
      json: 'string'
    };
    describe('#apiGatewayGetChangeReviewStepFromJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetChangeReviewStepFromJson(apiGatewayApiGatewayGetChangeReviewStepFromJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetChangeReviewStepFromJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetChangeReviewStepFromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetChangeReviewStepJsonBodyParam = {
      step: {
        step: null
      }
    };
    describe('#apiGatewayGetChangeReviewStepJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetChangeReviewStepJson(apiGatewayApiGatewayGetChangeReviewStepJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetChangeReviewStepJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetChangeReviewStepJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetCloudFetchSettingsBodyParam = {};
    describe('#apiGatewayGetCloudFetchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetCloudFetchSettings(apiGatewayApiGatewayGetCloudFetchSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetCloudFetchSettings', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetCloudFetchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetCloudFetchStatusBodyParam = {};
    describe('#apiGatewayGetCloudFetchStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetCloudFetchStatus(apiGatewayApiGatewayGetCloudFetchStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetCloudFetchStatus', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetCloudFetchStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetDataRetentionPolicyBodyParam = {};
    describe('#apiGatewayGetDataRetentionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetDataRetentionPolicy(apiGatewayApiGatewayGetDataRetentionPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetDataRetentionPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetDataRetentionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetDebugSnapshotsBodyParam = {
      network: 'string',
      configs: true
    };
    describe('#apiGatewayGetDebugSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetDebugSnapshots(apiGatewayApiGatewayGetDebugSnapshotsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetDebugSnapshots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetDebugSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetInsightBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      insightId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetInsight - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetInsight(apiGatewayApiGatewayGetInsightBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetInsight', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetInsight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetInsightResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      resultId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetInsightResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetInsightResult(apiGatewayApiGatewayGetInsightResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetInsightResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetInsightResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetLicenseMetadataBodyParam = {
      user: {
        uid: 'string',
        accountId: 'string'
      }
    };
    describe('#apiGatewayGetLicenseMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetLicenseMetadata(apiGatewayApiGatewayGetLicenseMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetLicenseMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetLicenseMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetLicenseUserBodyParam = {};
    describe('#apiGatewayGetLicenseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetLicenseUser(apiGatewayApiGatewayGetLicenseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetLicenseUser', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetLicenseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetNetworkAggregatesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#apiGatewayGetNetworkAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetNetworkAggregates(apiGatewayApiGatewayGetNetworkAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetNetworkAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetNetworkAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetNetworkMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#apiGatewayGetNetworkMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetNetworkMetadata(apiGatewayApiGatewayGetNetworkMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetNetworkMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetNetworkMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetPolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policyId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetPolicy(apiGatewayApiGatewayGetPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetPolicyResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: null
    };
    describe('#apiGatewayGetPolicyResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetPolicyResult(apiGatewayApiGatewayGetPolicyResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetPolicyResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetPolicyResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetQuestionBodyParam = {
      networkName: 'string',
      questionName: 'string'
    };
    describe('#apiGatewayGetQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetQuestion(apiGatewayApiGatewayGetQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetQuestionsBodyParam = {
      networkName: 'string',
      legacyVerbose: true
    };
    describe('#apiGatewayGetQuestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetQuestions(apiGatewayApiGatewayGetQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetQuestions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string'
    };
    describe('#apiGatewayGetReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetReferenceBook(apiGatewayApiGatewayGetReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetReferenceLibraryBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayGetReferenceLibrary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetReferenceLibrary(apiGatewayApiGatewayGetReferenceLibraryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetReferenceLibrary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetReferenceLibrary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonConfigurationsSummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 8,
        maxRows: 1,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#apiGatewayGetSnapshotComparisonConfigurationsSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonConfigurationsSummary(apiGatewayApiGatewayGetSnapshotComparisonConfigurationsSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonConfigurationsSummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonConfigurationsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonDevicesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 8,
        maxRows: 8,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonDevices(apiGatewayApiGatewayGetSnapshotComparisonDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonDevices', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonInterfacesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 4,
        maxRows: 1,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonInterfaces(apiGatewayApiGatewayGetSnapshotComparisonInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonInterfaces', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string'
    };
    describe('#apiGatewayGetSnapshotComparisonMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonMetadata(apiGatewayApiGatewayGetSnapshotComparisonMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonReachabilityBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 2,
        maxRows: 1,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      },
      aggregationType: 3,
      differenceType: 0,
      headerJson: 'string',
      filterType: 1,
      maxRows: 8,
      maxColumns: 10,
      debugParams: [
        'string'
      ]
    };
    describe('#apiGatewayGetSnapshotComparisonReachability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonReachability(apiGatewayApiGatewayGetSnapshotComparisonReachabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonReachability', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonReachability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutesSummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 3,
        maxRows: 5,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutesSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutesSummary(apiGatewayApiGatewayGetSnapshotComparisonRoutesSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutesSummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 6,
        maxRows: 7,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 4,
        maxRows: 2,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterfaceBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 3,
        maxRows: 10,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface(apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighborBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 6,
        maxRows: 4,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor(apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighborBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfNeighbor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcessBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 9,
        maxRows: 8,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess(apiGatewayApiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcessBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotComparisonRoutingProtocolsOspfProcess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotErrorsBodyParam = {
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#apiGatewayGetSnapshotErrors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotErrors(apiGatewayApiGatewayGetSnapshotErrorsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotErrors', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotErrors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotInputObjectBodyParam = {
      networkName: 'string',
      snapshotName: 'string',
      key: 'string'
    };
    describe('#apiGatewayGetSnapshotInputObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotInputObject(apiGatewayApiGatewayGetSnapshotInputObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotInputObject', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotInputObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#apiGatewayGetSnapshotMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotMetadata(apiGatewayApiGatewayGetSnapshotMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetSnapshotStatusBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#apiGatewayGetSnapshotStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetSnapshotStatus(apiGatewayApiGatewayGetSnapshotStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetSnapshotStatus', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetSnapshotStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetStepPreviewResultBodyParam = {
      previewId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayGetStepPreviewResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetStepPreviewResult(apiGatewayApiGatewayGetStepPreviewResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetStepPreviewResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetStepPreviewResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetTopologyAggregatesBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayGetTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetTopologyAggregates(apiGatewayApiGatewayGetTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetTopologyLayoutBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      expandedAggregates: [
        'string'
      ]
    };
    describe('#apiGatewayGetTopologyLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetTopologyLayout(apiGatewayApiGatewayGetTopologyLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetTopologyLayout', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetTopologyLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetTopologyPositionsBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayGetTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetTopologyPositions(apiGatewayApiGatewayGetTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetTopologyRootsBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayGetTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetTopologyRoots(apiGatewayApiGatewayGetTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetTopologySummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#apiGatewayGetTopologySummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetTopologySummary(apiGatewayApiGatewayGetTopologySummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetTopologySummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetTopologySummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayGetWebAccessTokenBodyParam = {
      uid: {
        uid: 'string',
        accountId: 'string'
      }
    };
    describe('#apiGatewayGetWebAccessToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayGetWebAccessToken(apiGatewayApiGatewayGetWebAccessTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayGetWebAccessToken', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayGetWebAccessToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayInitSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      zipData: [],
      debugFlags: [
        'string'
      ],
      disableParseReuse: false,
      nonce: 1,
      files: [
        {
          key: 'string',
          contents: []
        }
      ]
    };
    describe('#apiGatewayInitSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayInitSnapshot(apiGatewayApiGatewayInitSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayInitSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayInitSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayInitSnapshotComparisonBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string'
    };
    describe('#apiGatewayInitSnapshotComparison - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayInitSnapshotComparison(apiGatewayApiGatewayInitSnapshotComparisonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayInitSnapshotComparison', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayInitSnapshotComparison', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListAdHocAssertionMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#apiGatewayListAdHocAssertionMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListAdHocAssertionMetadata(apiGatewayApiGatewayListAdHocAssertionMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListAdHocAssertionMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListAdHocAssertionMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListChangeReviewsMetadataBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayListChangeReviewsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListChangeReviewsMetadata(apiGatewayApiGatewayListChangeReviewsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListChangeReviewsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListChangeReviewsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListInsightResultsMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      filter: {
        status: 3
      },
      pageSize: 3,
      pageToken: 'string'
    };
    describe('#apiGatewayListInsightResultsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListInsightResultsMetadata(apiGatewayApiGatewayListInsightResultsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListInsightResultsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListInsightResultsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListInsightsBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      pageSize: 3,
      pageToken: 'string'
    };
    describe('#apiGatewayListInsights - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListInsights(apiGatewayApiGatewayListInsightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListInsights', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListInsights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListNetworkMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      }
    };
    describe('#apiGatewayListNetworkMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListNetworkMetadata(apiGatewayApiGatewayListNetworkMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListNetworkMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListNetworkMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListPoliciesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      pageSize: 9,
      pageToken: 'string'
    };
    describe('#apiGatewayListPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListPolicies(apiGatewayApiGatewayListPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListPolicies', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListPolicyResultsMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      filter: {
        status: 2
      },
      pageSize: 6,
      pageToken: 'string'
    };
    describe('#apiGatewayListPolicyResultsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListPolicyResultsMetadata(apiGatewayApiGatewayListPolicyResultsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListPolicyResultsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListPolicyResultsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListQuestionsBodyParam = {
      networkName: 'string'
    };
    describe('#apiGatewayListQuestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListQuestions(apiGatewayApiGatewayListQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListQuestions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListSnapshotMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#apiGatewayListSnapshotMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListSnapshotMetadata(apiGatewayApiGatewayListSnapshotMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListSnapshotMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListSnapshotMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayListSnapshotsBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#apiGatewayListSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayListSnapshots(apiGatewayApiGatewayListSnapshotsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayListSnapshots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayListSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayParseBodyParam = {
      filename: 'string',
      contents: []
    };
    describe('#apiGatewayParse - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayParse(apiGatewayApiGatewayParseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayParse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayParse', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayParseChangeReviewStepBodyParam = {
      stepText: 'string'
    };
    describe('#apiGatewayParseChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayParseChangeReviewStep(apiGatewayApiGatewayParseChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayParseChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayParseChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutNetworkAggregatesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      aggregates: {
        aggregates: [
          {
            name: 'string',
            patterns: [
              'string'
            ],
            children: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#apiGatewayPutNetworkAggregates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayPutNetworkAggregates(apiGatewayApiGatewayPutNetworkAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutNetworkAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutNetworkAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutQuestionBodyParam = {
      networkName: 'string',
      question: 'string'
    };
    describe('#apiGatewayPutQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayPutQuestion(apiGatewayApiGatewayPutQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string',
      content: 'string'
    };
    describe('#apiGatewayPutReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayPutReferenceBook(apiGatewayApiGatewayPutReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutTopologyAggregatesBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#apiGatewayPutTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayPutTopologyAggregates(apiGatewayApiGatewayPutTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutTopologyPositionsBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#apiGatewayPutTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayPutTopologyPositions(apiGatewayApiGatewayPutTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayPutTopologyRootsBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#apiGatewayPutTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayPutTopologyRoots(apiGatewayApiGatewayPutTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayPutTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayPutTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayRunAllStepsBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#apiGatewayRunAllSteps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayRunAllSteps(apiGatewayApiGatewayRunAllStepsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayRunAllSteps', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayRunAllSteps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayRunChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#apiGatewayRunChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayRunChangeReviewStep(apiGatewayApiGatewayRunChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayRunChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayRunChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateAwsAccountBodyParam = {
      account: {
        name: 'string',
        roleArn: 'string',
        regions: [
          'string'
        ],
        accountId: 'string'
      }
    };
    describe('#apiGatewayUpdateAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateAwsAccount(apiGatewayApiGatewayUpdateAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateChangeReviewBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      },
      title: 'string',
      description: 'string',
      base: null,
      changeSnapshotName: 'string',
      change: {
        fileChanges: [
          {
            fileName: 'string',
            changeText: 'string'
          }
        ],
        newFiles: [
          {
            fileName: 'string',
            content: 'string'
          }
        ],
        deactivateDevices: [
          'string'
        ],
        deactivateInterfaces: [
          {
            hostname: 'string',
            interface: 'string'
          }
        ]
      }
    };
    describe('#apiGatewayUpdateChangeReview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateChangeReview(apiGatewayApiGatewayUpdateChangeReviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateChangeReview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateChangeReview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      },
      step: null
    };
    describe('#apiGatewayUpdateChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateChangeReviewStep(apiGatewayApiGatewayUpdateChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateCloudFetchPollIntervalBodyParam = {
      pollInterval: 9
    };
    describe('#apiGatewayUpdateCloudFetchPollInterval - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateCloudFetchPollInterval(apiGatewayApiGatewayUpdateCloudFetchPollIntervalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateCloudFetchPollInterval', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateCloudFetchPollInterval', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateCloudFetchSettingsBodyParam = {
      settings: {
        aws: {
          accounts: [
            {
              name: 'string',
              roleArn: 'string',
              regions: [
                'string'
              ],
              accountId: 'string'
            }
          ],
          partition: 1
        },
        pollInterval: 8
      }
    };
    describe('#apiGatewayUpdateCloudFetchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayUpdateCloudFetchSettings(apiGatewayApiGatewayUpdateCloudFetchSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateCloudFetchSettings', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateCloudFetchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateDataRetentionPolicyBodyParam = {
      policy: {
        maxAge: {
          low: 0,
          high: 0,
          unsigned: true
        },
        enforcementInterval: {
          low: 0,
          high: 0,
          unsigned: true
        }
      }
    };
    describe('#apiGatewayUpdateDataRetentionPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateDataRetentionPolicy(apiGatewayApiGatewayUpdateDataRetentionPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateDataRetentionPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateDataRetentionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayUpdateStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      },
      step: {
        step: null
      }
    };
    describe('#apiGatewayUpdateStepPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.apiGatewayUpdateStepPreview(apiGatewayApiGatewayUpdateStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayUpdateStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayUpdateStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /*
    const apiGatewayApiGatewayWorkMgrV1BodyParam = {
      credentials: {
        apiKey: 'string'
      },
      resource: 'string',
      params: 'string',
      fileData: [],
      fileParam: 'string',
      respContentType: 'string'
    };
    describe('#apiGatewayWorkMgrV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayWorkMgrV1(apiGatewayApiGatewayWorkMgrV1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayWorkMgrV1', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayWorkMgrV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const apiGatewayApiGatewayWorkMgrV2BodyParam = {
      credentials: {
        apiKey: 'string'
      },
      method: 'string',
      path: 'string',
      params: 'string',
      body: [],
      reqContentType: 'string',
      respContentType: 'string'
    };
    describe('#apiGatewayWorkMgrV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.apiGatewayWorkMgrV2(apiGatewayApiGatewayWorkMgrV2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-apiGatewayWorkMgrV2', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApiGateway', 'apiGatewayWorkMgrV2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    */

    const snapshotServiceSnapshotServiceCreateAdHocAssertionBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      assertion: {
        id: {
          uid: 'string'
        },
        input: {
          input: null,
          description: 'string',
          title: 'string'
        }
      },
      options: {
        hidden: false
      }
    };
    describe('#snapshotServiceCreateAdHocAssertion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceCreateAdHocAssertion(snapshotServiceSnapshotServiceCreateAdHocAssertionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceCreateAdHocAssertion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceCreateAdHocAssertion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceCreateNetworkBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      nonce: 5
    };
    describe('#snapshotServiceCreateNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceCreateNetwork(snapshotServiceSnapshotServiceCreateNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceCreateNetwork', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceCreateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceCreatePolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policy: {
        uid: {
          uid: 'string'
        },
        input: {
          input: null,
          description: 'string',
          title: 'string'
        }
      }
    };
    describe('#snapshotServiceCreatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceCreatePolicy(snapshotServiceSnapshotServiceCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceCreatePolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceCreatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteNetworkBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      nonce: 3
    };
    describe('#snapshotServiceDeleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceDeleteNetwork(snapshotServiceSnapshotServiceDeleteNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteNetwork', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeletePolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policyId: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceDeletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceDeletePolicy(snapshotServiceSnapshotServiceDeletePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeletePolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteQuestionBodyParam = {
      networkName: 'string',
      questionName: 'string'
    };
    describe('#snapshotServiceDeleteQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceDeleteQuestion(snapshotServiceSnapshotServiceDeleteQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string'
    };
    describe('#snapshotServiceDeleteReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceDeleteReferenceBook(snapshotServiceSnapshotServiceDeleteReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      nonce: 4
    };
    describe('#snapshotServiceDeleteSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceDeleteSnapshot(snapshotServiceSnapshotServiceDeleteSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteTopologyAggregatesBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceDeleteTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceDeleteTopologyAggregates(snapshotServiceSnapshotServiceDeleteTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteTopologyPositionsBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceDeleteTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceDeleteTopologyPositions(snapshotServiceSnapshotServiceDeleteTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceDeleteTopologyRootsBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceDeleteTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceDeleteTopologyRoots(snapshotServiceSnapshotServiceDeleteTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceDeleteTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceDeleteTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceExportAssertionInputBodyParam = {
      assertionInput: {
        input: null,
        description: 'string',
        title: 'string'
      }
    };
    describe('#snapshotServiceExportAssertionInput - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceExportAssertionInput(snapshotServiceSnapshotServiceExportAssertionInputBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceExportAssertionInput', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceExportAssertionInput', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceForkFromMasterSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      nonce: 6,
      debugFlags: [
        'string'
      ],
      disableParseReuse: true,
      files: [
        {
          key: 'string',
          contents: []
        }
      ],
      removeFiles: [
        {
          key: 'string'
        }
      ],
      removeDirs: [
        {
          key: 'string'
        }
      ]
    };
    describe('#snapshotServiceForkFromMasterSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceForkFromMasterSnapshot(snapshotServiceSnapshotServiceForkFromMasterSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceForkFromMasterSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceForkFromMasterSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceForkSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      zipData: [],
      debugFlags: [
        'string'
      ],
      disableParseReuse: false,
      nonce: 1,
      baseSnapshotName: 'string',
      deactivateNodes: [
        {
          name: 'string'
        }
      ],
      restoreNodes: [
        {
          name: 'string'
        }
      ],
      deactivateInterfaces: [
        {
          nodeName: 'string',
          interfaceName: 'string'
        }
      ],
      restoreInterfaces: [
        {
          nodeName: 'string',
          interfaceName: 'string'
        }
      ],
      files: [
        {
          key: 'string',
          contents: []
        }
      ],
      options: {
        hidden: false
      }
    };
    describe('#snapshotServiceForkSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceForkSnapshot(snapshotServiceSnapshotServiceForkSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceForkSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceForkSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceForkSnapshotWithChangeBodyParam = {
      networkName: 'string',
      snapshotName: 'string',
      baseSnapshotName: 'string',
      change: {
        fileChanges: [
          {
            fileName: 'string',
            changeText: 'string'
          }
        ],
        newFiles: [
          {
            fileName: 'string',
            content: 'string'
          }
        ],
        deactivateDevices: [
          'string'
        ],
        deactivateInterfaces: [
          {
            hostname: 'string',
            interface: 'string'
          }
        ]
      },
      debugFlags: [
        'string'
      ],
      disableParseReuse: true,
      nonce: 9,
      options: {
        hidden: false
      }
    };
    describe('#snapshotServiceForkSnapshotWithChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceForkSnapshotWithChange(snapshotServiceSnapshotServiceForkSnapshotWithChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceForkSnapshotWithChange', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceForkSnapshotWithChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetAdHocAssertionBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceGetAdHocAssertion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetAdHocAssertion(snapshotServiceSnapshotServiceGetAdHocAssertionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetAdHocAssertion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetAdHocAssertion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetAdHocAssertionResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceGetAdHocAssertionResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetAdHocAssertionResult(snapshotServiceSnapshotServiceGetAdHocAssertionResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetAdHocAssertionResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetAdHocAssertionResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetAssertionInputJsonBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      assertionInput: {
        input: null,
        description: 'string',
        title: 'string'
      }
    };
    describe('#snapshotServiceGetAssertionInputJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetAssertionInputJson(snapshotServiceSnapshotServiceGetAssertionInputJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetAssertionInputJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetAssertionInputJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetDataRetentionPolicyBodyParam = {};
    describe('#snapshotServiceGetDataRetentionPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetDataRetentionPolicy(snapshotServiceSnapshotServiceGetDataRetentionPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetDataRetentionPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetDataRetentionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetDebugSnapshotsBodyParam = {
      network: 'string',
      configs: true
    };
    describe('#snapshotServiceGetDebugSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetDebugSnapshots(snapshotServiceSnapshotServiceGetDebugSnapshotsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetDebugSnapshots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetDebugSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetInsightBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      insightId: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceGetInsight - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetInsight(snapshotServiceSnapshotServiceGetInsightBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetInsight', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetInsight', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetInsightResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      resultId: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceGetInsightResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetInsightResult(snapshotServiceSnapshotServiceGetInsightResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetInsightResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetInsightResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetNetworkAggregatesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#snapshotServiceGetNetworkAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetNetworkAggregates(snapshotServiceSnapshotServiceGetNetworkAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetNetworkAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetNetworkAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetNetworkIdBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceGetNetworkId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetNetworkId(snapshotServiceSnapshotServiceGetNetworkIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetNetworkId', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetNetworkId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetNetworkMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#snapshotServiceGetNetworkMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetNetworkMetadata(snapshotServiceSnapshotServiceGetNetworkMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetNetworkMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetNetworkMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetPolicyBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      policyId: {
        uid: 'string'
      }
    };
    describe('#snapshotServiceGetPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetPolicy(snapshotServiceSnapshotServiceGetPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetPolicyResultBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      id: null
    };
    describe('#snapshotServiceGetPolicyResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetPolicyResult(snapshotServiceSnapshotServiceGetPolicyResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetPolicyResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetPolicyResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetQuestionBodyParam = {
      networkName: 'string',
      questionName: 'string'
    };
    describe('#snapshotServiceGetQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetQuestion(snapshotServiceSnapshotServiceGetQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetQuestionsBodyParam = {
      networkName: 'string',
      legacyVerbose: true
    };
    describe('#snapshotServiceGetQuestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetQuestions(snapshotServiceSnapshotServiceGetQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetQuestions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string'
    };
    describe('#snapshotServiceGetReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetReferenceBook(snapshotServiceSnapshotServiceGetReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetReferenceLibraryBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceGetReferenceLibrary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetReferenceLibrary(snapshotServiceSnapshotServiceGetReferenceLibraryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetReferenceLibrary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetReferenceLibrary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonConfigurationsSummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 7,
        maxRows: 7,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonConfigurationsSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonConfigurationsSummary(snapshotServiceSnapshotServiceGetSnapshotComparisonConfigurationsSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonConfigurationsSummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonConfigurationsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonDevicesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 3,
        maxRows: 10,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonDevices(snapshotServiceSnapshotServiceGetSnapshotComparisonDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonDevices', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonInterfacesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 5,
        maxRows: 6,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonInterfaces(snapshotServiceSnapshotServiceGetSnapshotComparisonInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonInterfaces', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string'
    };
    describe('#snapshotServiceGetSnapshotComparisonMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonMetadata(snapshotServiceSnapshotServiceGetSnapshotComparisonMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonReachabilityBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 8,
        maxRows: 5,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      },
      aggregationType: 3,
      differenceType: 1,
      headerJson: 'string',
      filterType: 1,
      maxRows: 8,
      maxColumns: 8,
      debugParams: [
        'string'
      ]
    };
    describe('#snapshotServiceGetSnapshotComparisonReachability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonReachability(snapshotServiceSnapshotServiceGetSnapshotComparisonReachabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonReachability', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonReachability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutesSummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 5,
        maxRows: 8,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: true
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutesSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutesSummary(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutesSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutesSummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 1,
        maxRows: 3,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpPeerAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 9,
        maxRows: 9,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutingProtocolsBgpProcessAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterfaceBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 3,
        maxRows: 6,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterfaceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighborBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 10,
        maxRows: 1,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: false
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighborBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfNeighbor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcessBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string',
      answerOptions: {
        rowOffset: 5,
        maxRows: 5,
        columns: [
          'string'
        ],
        sortOrder: [
          {
            column: 'string',
            reversed: false
          }
        ],
        filters: [
          {
            column: 'string',
            filterText: 'string'
          }
        ],
        uniqueRows: true
      }
    };
    describe('#snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess(snapshotServiceSnapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcessBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotComparisonRoutingProtocolsOspfProcess', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotErrorsBodyParam = {
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#snapshotServiceGetSnapshotErrors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotErrors(snapshotServiceSnapshotServiceGetSnapshotErrorsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotErrors', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotErrors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotIdBodyParam = {
      networkId: {
        name: 'string',
        id: 1
      },
      snapshotName: 'string'
    };
    describe('#snapshotServiceGetSnapshotId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotId(snapshotServiceSnapshotServiceGetSnapshotIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotId', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotInputObjectBodyParam = {
      networkName: 'string',
      snapshotName: 'string',
      key: 'string'
    };
    describe('#snapshotServiceGetSnapshotInputObject - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotInputObject(snapshotServiceSnapshotServiceGetSnapshotInputObjectBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotInputObject', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotInputObject', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#snapshotServiceGetSnapshotMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotMetadata(snapshotServiceSnapshotServiceGetSnapshotMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetSnapshotStatusBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#snapshotServiceGetSnapshotStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetSnapshotStatus(snapshotServiceSnapshotServiceGetSnapshotStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetSnapshotStatus', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetSnapshotStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetTopologyAggregatesBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceGetTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetTopologyAggregates(snapshotServiceSnapshotServiceGetTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetTopologyLayoutBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      expandedAggregates: [
        'string'
      ]
    };
    describe('#snapshotServiceGetTopologyLayout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetTopologyLayout(snapshotServiceSnapshotServiceGetTopologyLayoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetTopologyLayout', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetTopologyLayout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetTopologyPositionsBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceGetTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetTopologyPositions(snapshotServiceSnapshotServiceGetTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetTopologyRootsBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceGetTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetTopologyRoots(snapshotServiceSnapshotServiceGetTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceGetTopologySummaryBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#snapshotServiceGetTopologySummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceGetTopologySummary(snapshotServiceSnapshotServiceGetTopologySummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceGetTopologySummary', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceGetTopologySummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceInitSnapshotBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      zipData: [],
      debugFlags: [
        'string'
      ],
      disableParseReuse: true,
      nonce: 5,
      files: [
        {
          key: 'string',
          contents: []
        }
      ]
    };
    describe('#snapshotServiceInitSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceInitSnapshot(snapshotServiceSnapshotServiceInitSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceInitSnapshot', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceInitSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceInitSnapshotComparisonBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      referenceSnapshotName: 'string'
    };
    describe('#snapshotServiceInitSnapshotComparison - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceInitSnapshotComparison(snapshotServiceSnapshotServiceInitSnapshotComparisonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceInitSnapshotComparison', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceInitSnapshotComparison', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListAdHocAssertionMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string'
    };
    describe('#snapshotServiceListAdHocAssertionMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListAdHocAssertionMetadata(snapshotServiceSnapshotServiceListAdHocAssertionMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListAdHocAssertionMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListAdHocAssertionMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListInsightResultsMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      filter: {
        status: 0
      },
      pageSize: 1,
      pageToken: 'string'
    };
    describe('#snapshotServiceListInsightResultsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListInsightResultsMetadata(snapshotServiceSnapshotServiceListInsightResultsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListInsightResultsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListInsightResultsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListInsightsBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      pageSize: 1,
      pageToken: 'string'
    };
    describe('#snapshotServiceListInsights - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListInsights(snapshotServiceSnapshotServiceListInsightsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListInsights', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListInsights', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListNetworkMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      }
    };
    describe('#snapshotServiceListNetworkMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListNetworkMetadata(snapshotServiceSnapshotServiceListNetworkMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListNetworkMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListNetworkMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListPoliciesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      pageSize: 9,
      pageToken: 'string'
    };
    describe('#snapshotServiceListPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListPolicies(snapshotServiceSnapshotServiceListPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListPolicies', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListPolicyResultsMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      snapshotName: 'string',
      filter: {
        status: 0
      },
      pageSize: 9,
      pageToken: 'string'
    };
    describe('#snapshotServiceListPolicyResultsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListPolicyResultsMetadata(snapshotServiceSnapshotServiceListPolicyResultsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListPolicyResultsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListPolicyResultsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListQuestionsBodyParam = {
      networkName: 'string'
    };
    describe('#snapshotServiceListQuestions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListQuestions(snapshotServiceSnapshotServiceListQuestionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListQuestions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListQuestions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListSnapshotMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#snapshotServiceListSnapshotMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListSnapshotMetadata(snapshotServiceSnapshotServiceListSnapshotMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListSnapshotMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListSnapshotMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceListSnapshotsBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string'
    };
    describe('#snapshotServiceListSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServiceListSnapshots(snapshotServiceSnapshotServiceListSnapshotsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceListSnapshots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceListSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutNetworkAggregatesBodyParam = {
      credentials: {
        apiKey: 'string'
      },
      networkName: 'string',
      aggregates: {
        aggregates: [
          {
            name: 'string',
            patterns: [
              'string'
            ],
            children: [
              'string'
            ]
          }
        ]
      }
    };
    describe('#snapshotServicePutNetworkAggregates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServicePutNetworkAggregates(snapshotServiceSnapshotServicePutNetworkAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutNetworkAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutNetworkAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutQuestionBodyParam = {
      networkName: 'string',
      question: 'string'
    };
    describe('#snapshotServicePutQuestion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServicePutQuestion(snapshotServiceSnapshotServicePutQuestionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutQuestion', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutQuestion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutReferenceBookBodyParam = {
      networkName: 'string',
      bookName: 'string',
      content: 'string'
    };
    describe('#snapshotServicePutReferenceBook - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServicePutReferenceBook(snapshotServiceSnapshotServicePutReferenceBookBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutReferenceBook', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutReferenceBook', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutTopologyAggregatesBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#snapshotServicePutTopologyAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServicePutTopologyAggregates(snapshotServiceSnapshotServicePutTopologyAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutTopologyAggregates', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutTopologyAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutTopologyPositionsBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#snapshotServicePutTopologyPositions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServicePutTopologyPositions(snapshotServiceSnapshotServicePutTopologyPositionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutTopologyPositions', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutTopologyPositions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServicePutTopologyRootsBodyParam = {
      networkName: 'string',
      content: 'string'
    };
    describe('#snapshotServicePutTopologyRoots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snapshotServicePutTopologyRoots(snapshotServiceSnapshotServicePutTopologyRootsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServicePutTopologyRoots', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServicePutTopologyRoots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotServiceSnapshotServiceUpdateDataRetentionPolicyBodyParam = {
      policy: {
        maxAge: {
          low: 0,
          high: 0,
          unsigned: true
        },
        enforcementInterval: {
          low: 0,
          high: 0,
          unsigned: true
        }
      }
    };
    describe('#snapshotServiceUpdateDataRetentionPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.snapshotServiceUpdateDataRetentionPolicy(snapshotServiceSnapshotServiceUpdateDataRetentionPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-snapshotServiceUpdateDataRetentionPolicy', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SnapshotService', 'snapshotServiceUpdateDataRetentionPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseLicenseActivateLicenseBodyParam = {
      activationCode: []
    };
    describe('#licenseActivateLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.licenseActivateLicense(licenseLicenseActivateLicenseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-licenseActivateLicense', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'licenseActivateLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseLicenseGetLicenseMetadataBodyParam = {
      user: {
        uid: 'string',
        accountId: 'string'
      }
    };
    describe('#licenseGetLicenseMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.licenseGetLicenseMetadata(licenseLicenseGetLicenseMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-licenseGetLicenseMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'licenseGetLicenseMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseLicenseGetLicenseUserBodyParam = {};
    describe('#licenseGetLicenseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.licenseGetLicenseUser(licenseLicenseGetLicenseUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-licenseGetLicenseUser', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'licenseGetLicenseUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchAddAwsAccountBodyParam = {
      account: {
        name: 'string',
        roleArn: 'string',
        regions: [
          'string'
        ],
        accountId: 'string'
      }
    };
    describe('#cloudFetchAddAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloudFetchAddAwsAccount(cloudFetchCloudFetchAddAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchAddAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchAddAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchDeleteAwsAccountBodyParam = {
      accountId: 'string'
    };
    describe('#cloudFetchDeleteAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloudFetchDeleteAwsAccount(cloudFetchCloudFetchDeleteAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchDeleteAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchDeleteAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchGetAwsAccountsStatusesBodyParam = {};
    describe('#cloudFetchGetAwsAccountsStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudFetchGetAwsAccountsStatuses(cloudFetchCloudFetchGetAwsAccountsStatusesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchGetAwsAccountsStatuses', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchGetAwsAccountsStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchGetCloudFetchSettingsBodyParam = {};
    describe('#cloudFetchGetCloudFetchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudFetchGetCloudFetchSettings(cloudFetchCloudFetchGetCloudFetchSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchGetCloudFetchSettings', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchGetCloudFetchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchGetCloudFetchStatusBodyParam = {};
    describe('#cloudFetchGetCloudFetchStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudFetchGetCloudFetchStatus(cloudFetchCloudFetchGetCloudFetchStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchGetCloudFetchStatus', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchGetCloudFetchStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchUpdateAwsAccountBodyParam = {
      account: {
        name: 'string',
        roleArn: 'string',
        regions: [
          'string'
        ],
        accountId: 'string'
      }
    };
    describe('#cloudFetchUpdateAwsAccount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloudFetchUpdateAwsAccount(cloudFetchCloudFetchUpdateAwsAccountBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchUpdateAwsAccount', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchUpdateAwsAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchUpdateCloudFetchPollIntervalBodyParam = {
      pollInterval: 2
    };
    describe('#cloudFetchUpdateCloudFetchPollInterval - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloudFetchUpdateCloudFetchPollInterval(cloudFetchCloudFetchUpdateCloudFetchPollIntervalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchUpdateCloudFetchPollInterval', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchUpdateCloudFetchPollInterval', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudFetchCloudFetchUpdateCloudFetchSettingsBodyParam = {
      settings: {
        aws: {
          accounts: [
            {
              name: 'string',
              roleArn: 'string',
              regions: [
                'string'
              ],
              accountId: 'string'
            }
          ],
          partition: 0
        },
        pollInterval: 9
      }
    };
    describe('#cloudFetchUpdateCloudFetchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudFetchUpdateCloudFetchSettings(cloudFetchCloudFetchUpdateCloudFetchSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-cloudFetchUpdateCloudFetchSettings', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudFetch', 'cloudFetchUpdateCloudFetchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authAuthGetAuthorizationMetadataBodyParam = {
      credentials: {
        apiKey: 'string'
      }
    };
    describe('#authGetAuthorizationMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authGetAuthorizationMetadata(authAuthGetAuthorizationMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-authGetAuthorizationMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'authGetAuthorizationMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authAuthGetWebAccessTokenBodyParam = {
      uid: {
        uid: 'string',
        accountId: 'string'
      }
    };
    describe('#authGetWebAccessToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authGetWebAccessToken(authAuthGetWebAccessTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-authGetWebAccessToken', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'authGetWebAccessToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationCommitStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      },
      stepId: {
        uid: 'string'
      }
    };
    describe('#changeValidationCommitStepPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationCommitStepPreview(changeValidationChangeValidationCommitStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationCommitStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationCommitStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationCreateChangeReviewBodyParam = {
      title: 'string',
      description: 'string',
      networkName: 'string',
      base: null,
      changeSnapshotName: 'string',
      change: {
        fileChanges: [
          {
            fileName: 'string',
            changeText: 'string'
          }
        ],
        newFiles: [
          {
            fileName: 'string',
            content: 'string'
          }
        ],
        deactivateDevices: [
          'string'
        ],
        deactivateInterfaces: [
          {
            hostname: 'string',
            interface: 'string'
          }
        ]
      },
      steps: [
        {
          step: null
        }
      ]
    };
    describe('#changeValidationCreateChangeReview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationCreateChangeReview(changeValidationChangeValidationCreateChangeReviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationCreateChangeReview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationCreateChangeReview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationCreateChangeReviewStepBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      },
      step: null
    };
    describe('#changeValidationCreateChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationCreateChangeReviewStep(changeValidationChangeValidationCreateChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationCreateChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationCreateChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationCreateStepPreviewBodyParam = {
      changeReviewId: {
        uid: 'string',
        network: 'string'
      },
      step: {
        step: null
      }
    };
    describe('#changeValidationCreateStepPreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationCreateStepPreview(changeValidationChangeValidationCreateStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationCreateStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationCreateStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationDeleteChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#changeValidationDeleteChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationDeleteChangeReviewStep(changeValidationChangeValidationDeleteChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationDeleteChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationDeleteChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationDeleteStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      }
    };
    describe('#changeValidationDeleteStepPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationDeleteStepPreview(changeValidationChangeValidationDeleteStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationDeleteStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationDeleteStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationExportChangeReviewStepBodyParam = {
      step: {
        step: null
      }
    };
    describe('#changeValidationExportChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationExportChangeReviewStep(changeValidationChangeValidationExportChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationExportChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationExportChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetChangeReviewChangeBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#changeValidationGetChangeReviewChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetChangeReviewChange(changeValidationChangeValidationGetChangeReviewChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetChangeReviewChange', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetChangeReviewChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetChangeReviewMetadataBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#changeValidationGetChangeReviewMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetChangeReviewMetadata(changeValidationChangeValidationGetChangeReviewMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetChangeReviewMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetChangeReviewMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#changeValidationGetChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetChangeReviewStep(changeValidationChangeValidationGetChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetChangeReviewStepFromJsonBodyParam = {
      json: 'string'
    };
    describe('#changeValidationGetChangeReviewStepFromJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetChangeReviewStepFromJson(changeValidationChangeValidationGetChangeReviewStepFromJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetChangeReviewStepFromJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetChangeReviewStepFromJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetChangeReviewStepJsonBodyParam = {
      step: {
        step: null
      }
    };
    describe('#changeValidationGetChangeReviewStepJson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetChangeReviewStepJson(changeValidationChangeValidationGetChangeReviewStepJsonBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetChangeReviewStepJson', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetChangeReviewStepJson', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationGetStepPreviewResultBodyParam = {
      previewId: {
        uid: 'string'
      }
    };
    describe('#changeValidationGetStepPreviewResult - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationGetStepPreviewResult(changeValidationChangeValidationGetStepPreviewResultBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationGetStepPreviewResult', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationGetStepPreviewResult', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationListChangeReviewsMetadataBodyParam = {
      networkName: 'string'
    };
    describe('#changeValidationListChangeReviewsMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationListChangeReviewsMetadata(changeValidationChangeValidationListChangeReviewsMetadataBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationListChangeReviewsMetadata', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationListChangeReviewsMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationParseChangeReviewStepBodyParam = {
      stepText: 'string'
    };
    describe('#changeValidationParseChangeReviewStep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeValidationParseChangeReviewStep(changeValidationChangeValidationParseChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationParseChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationParseChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationRunAllStepsBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      }
    };
    describe('#changeValidationRunAllSteps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationRunAllSteps(changeValidationChangeValidationRunAllStepsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationRunAllSteps', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationRunAllSteps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationRunChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      }
    };
    describe('#changeValidationRunChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationRunChangeReviewStep(changeValidationChangeValidationRunChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationRunChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationRunChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationUpdateChangeReviewBodyParam = {
      reviewId: {
        uid: 'string',
        network: 'string'
      },
      title: 'string',
      description: 'string',
      base: null,
      changeSnapshotName: 'string',
      change: {
        fileChanges: [
          {
            fileName: 'string',
            changeText: 'string'
          }
        ],
        newFiles: [
          {
            fileName: 'string',
            content: 'string'
          }
        ],
        deactivateDevices: [
          'string'
        ],
        deactivateInterfaces: [
          {
            hostname: 'string',
            interface: 'string'
          }
        ]
      }
    };
    describe('#changeValidationUpdateChangeReview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationUpdateChangeReview(changeValidationChangeValidationUpdateChangeReviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationUpdateChangeReview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationUpdateChangeReview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationUpdateChangeReviewStepBodyParam = {
      stepId: {
        uid: 'string'
      },
      step: null
    };
    describe('#changeValidationUpdateChangeReviewStep - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationUpdateChangeReviewStep(changeValidationChangeValidationUpdateChangeReviewStepBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationUpdateChangeReviewStep', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationUpdateChangeReviewStep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeValidationChangeValidationUpdateStepPreviewBodyParam = {
      previewId: {
        uid: 'string'
      },
      step: {
        step: null
      }
    };
    describe('#changeValidationUpdateStepPreview - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeValidationUpdateStepPreview(changeValidationChangeValidationUpdateStepPreviewBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-changeValidationUpdateStepPreview', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeValidation', 'changeValidationUpdateStepPreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthHealthHealthCheckBodyParam = {
      service: 'string'
    };
    describe('#healthHealthCheck - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.healthHealthCheck(healthHealthHealthCheckBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-healthHealthCheck', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'healthHealthCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthHealthWatchBodyParam = {
      service: 'string'
    };
    describe('#healthWatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.healthWatch(healthHealthWatchBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-batfish-adapter-healthWatch', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'healthWatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
